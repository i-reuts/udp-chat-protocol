# Server

A UDP Chat server listening on port `4040` for incoming messages.

Processes messages and responds to clients.

Default port can be changed in `config\config.exs` file.

## Usage

To start the server, use `mix run --no-halt` command.
