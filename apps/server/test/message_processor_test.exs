defmodule MessageProcessorTest do
  use ExUnit.Case

  import Server.MessageProcessor

  alias Server.UserRegistry, as: Registry
  alias Server.Config
  alias Core.Messages.{RegisterMessage, RegisteredMessage, AlreadyRegisteredMessage, RegisterDeniedMessage, DeregisterMessage, DeregisteredMessage}
  alias Core.Messages.{PublishMessage, PublishedMessage, UnpublishedMessage}
  alias Core.Messages.{InformReqMessage, InformRespMessage}
  alias Core.Messages.{FindReqMessage, FindRespMessage, FindDeniedMessage, ReferMessage}
  alias Core.Messages.UserNotRegisteredMessage

  @registry_filename Application.get_env(:server, :registry_filename)

  # One time set-up and clean up
  setup_all do
   {:ok, _registry} = Registry.start_link()
   {:ok, _config} = Config.start_link()
    on_exit fn ->
      File.rm(@registry_filename)
    end
    :ok
  end

  # Clean up registry before each test
  setup do
    Registry.clear()
  end

  describe "when register message is recieved from a non-registered user" do
    setup do
      response = mock_register_message |> process
      {:ok, [response: response]}
    end

    test "user registration data is added to the registry" do
      assert Registry.get_user_info("John") === %{"ip_address" => "251.62.54.13"}
    end

    test "returns a registered message", context do
      assert context[:response] === {:ok, %RegisteredMessage{request_number: 0}}
    end

    test "registered users count is incremented" do
      assert Registry.get_num_registered_users() === 1
    end
  end

  describe "when register message is recieved from an already registered user" do
    test "returns an already_registered message" do
      # Register the user once
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Try to register again
      response = mock_register_message |> process
      assert response === {:ok, %AlreadyRegisteredMessage{request_number: 0}}
    end

    test "registered users count is not incremented" do
      assert Registry.get_num_registered_users() === 0
    end
  end

  describe "when register message is received and the server is full" do
    test "returns a register denied message" do
      # Set next server
      Config.set_next_server_address({{215,94,18,129}, 4356})

      # Register 5 users
      assert {:ok, %RegisteredMessage{request_number: 0}} === mock_register_message("test_user1") |> process
      assert {:ok, %RegisteredMessage{request_number: 0}} === mock_register_message("test_user2") |> process
      assert {:ok, %RegisteredMessage{request_number: 0}} === mock_register_message("test_user3") |> process
      assert {:ok, %RegisteredMessage{request_number: 0}} === mock_register_message("test_user4") |> process
      assert {:ok, %RegisteredMessage{request_number: 0}} === mock_register_message("test_user5") |> process

      # Try registering sixth user
      expected_response = %RegisterDeniedMessage{request_number: 0, ip_address: {215,94,18,129}, port: 4356}
      assert {:ok, expected_response} === mock_register_message("test_user6") |> process
    end
  end

  describe "when deregister message is recivede" do
    test "if user is registered deregisters user" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Deregister user
      response = mock_deregister_message |> process
      assert response === {:ok, %DeregisteredMessage{request_number: 0}}
      # Check registry
      assert Registry.get_user_info("John") === :user_not_found
    end

    test "if user not registered returns user_not_registered message" do
      # Deregister user
      response = mock_deregister_message |> process
      assert response === {:ok, %UserNotRegisteredMessage{request_number: 0, username: "John"}}
    end
  end

  describe "when publish message is recieved" do
    test "if user is registered updates the registry and returns published message" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Publish info
      response = mock_publish_message |> process
      assert response === {:ok, %PublishedMessage{request_number: 0, username: "John", port: 4040, status: "on", allowed_users: ["Jane", "Mark"]}}
      # Verify registry
      assert Registry.get_user_info("John")["port"]=== 4040
      assert Registry.get_user_info("John")["status"] === "on"
      assert Registry.get_user_info("John")["allowed_users"] === ["Jane", "Mark"]
    end

    test "if user is not registered returns user_not_registered message" do
      # Publish info
      response = mock_publish_message |> process
      assert response === {:ok, %UserNotRegisteredMessage{request_number: 0, username: "John"}}
    end
  end

  describe "when inform_req is recieved" do
    test "if user is registered returns inform_resp message" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Publish info
      response = mock_publish_message |> process
      assert response === {:ok, %PublishedMessage{request_number: 0, username: "John", port: 4040, status: "on", allowed_users: ["Jane", "Mark"]}}
      # Request info
      response = mock_inform_req_message |> process
      assert response === {:ok, %InformRespMessage{request_number: 0, username: "John", port: 4040, status: "on", allowed_users: ["Jane", "Mark"]}}
    end

    test "if user is not registered returns user_not_registered message" do
      # Request info
      response = mock_inform_req_message |> process
      assert response === {:ok, %UserNotRegisteredMessage{request_number: 0, username: "John"}}
    end

    test "if user is registered but has not published returns unpublished message" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Request info
      response = mock_inform_req_message |> process
      assert response === {:ok, %UnpublishedMessage{request_number: 0}}
    end
  end

  describe "when find_req is recieved" do
    test "if user is registered and sender is on allowed user list returns find_resp message" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Publish info
      response = mock_publish_message |> process
      assert response === {:ok, %PublishedMessage{request_number: 0, username: "John", port: 4040, status: "on", allowed_users: ["Jane", "Mark"]}}
      # Request info
      response = mock_find_req_message |> process
      assert response === {:ok, %FindRespMessage{request_number: 0, username: "John", port: 4040, ip_address: {251,62,54,13}}}
      
      # Publish off
      response = mock_publish_message("off") |> process
      assert response === {:ok, %PublishedMessage{request_number: 0, username: "John", port: 4040, status: "off", allowed_users: ["Jane", "Mark"]}}
      # Request info
      response = mock_find_req_message |> process
      assert response === {:ok, %FindRespMessage{request_number: 0, username: "John", status: "off"}}
    end

    test "if user is registered and sender is not on allowed user list returns find_denied message" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Publish info
      response = mock_publish_message |> process
      assert response === {:ok, %PublishedMessage{request_number: 0, username: "John", port: 4040, status: "on", allowed_users: ["Jane", "Mark"]}}
      # Request info
      response = mock_find_req_message("Perry") |> process
      assert response === {:ok, %FindDeniedMessage{request_number: 0, username: "John"}}
    end

    test "if user is not registered returns refer message" do
      # Set next server
      Config.set_next_server_address({{215,94,18,129}, 4356})
      # Request info
      response = mock_find_req_message |> process
      assert response === {:ok, %ReferMessage{request_number: 0, ip_address: {215,94,18,129}, port: 4356}}
    end

    test "if user is registered but has not published returns unpublished message" do
      # Register the user
      response = mock_register_message |> process
      assert response === {:ok, %RegisteredMessage{request_number: 0}}
      # Request info
      response = mock_find_req_message("Perry") |> process
      assert response === {:ok, %UnpublishedMessage{request_number: 0}}
    end
  end

  defp mock_register_message(username \\ "John") do
    %RegisterMessage{request_number: 0, username: username, ip_address: {251,62,54,13}}
  end

  defp mock_deregister_message(username \\ "John") do
    %DeregisterMessage{request_number: 0, username: username}
  end

  defp mock_publish_message(status \\ "on") do
    %PublishMessage{request_number: 0, username: "John", port: 4040, status: status, allowed_users: ["Jane", "Mark"]}
  end

  defp mock_inform_req_message() do
    %InformReqMessage{request_number: 0, username: "John"}
  end

  defp mock_find_req_message(username \\ "Jane") do
    %FindReqMessage{request_number: 0, sender_username: username, username: "John"}
  end

end
