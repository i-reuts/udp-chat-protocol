defmodule ServerTest do
  use ExUnit.Case

  alias Core.{MessageEncoder, MessageDecoder, UDP}
  alias Core.Messages.{RegisterMessage}

  @registry_filename Application.get_env(:server, :registry_filename)

  # One time set-up and clean up
  setup_all do
   Application.start(:server)
    on_exit fn ->
      Application.stop(:server)
      File.rm(@registry_filename)
    end
    :ok
  end

  test "server responds to a valid message" do
    # Build a message
    {:ok, message} = mock_register_message |> MessageEncoder.encode

    # Send to server
    {:ok, socket} = :gen_udp.open(6793, [:binary, active: false])
    {:ok, server_port} = UDP.get_active_port()
    :ok = :gen_udp.send(socket, {127,0,0,1}, server_port, message)

    # Recieve response
    {:ok, _data = {ip, port, message}} = :gen_tcp.recv(socket, 0, 1000)

    # Decode response
    {:ok, message} = MessageDecoder.decode({ip, port}, message)

    # Verify response
    assert message.request_number === mock_register_message.request_number
  end

  defp mock_register_message do
    %RegisterMessage{request_number: 0, username: "John"}
  end
end