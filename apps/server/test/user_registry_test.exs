defmodule UserRegistryTest do
  use ExUnit.Case

  import Server.UserRegistry

  @registry_filename Application.get_env(:server, :registry_filename)

  # Registry set-up and clean up for each test
  setup do
    create_mock_backup_file()

    {:ok, registry} = start()

    on_exit fn ->
      Agent.stop(registry)
      File.rm(@registry_filename)
    end 
  end

  describe "when back-up file exists on start-up" do
    test "registry is initialized from the back-up file" do
      assert get_user_info("Tony") === %{"ip_address" => "213.143.75.162", "port" => 15300, "status" => "On", "allowed_users" => ["Bob", "Bill"]}
    end
  end

  describe "when updating in-memory registry" do
    test "back-up file is updated" do
      # Registering a user
      :ok = register_user("Jack", {231,14,72,145})
      assert fetch_user_info_from_file("Jack") === %{"ip_address" => "231.14.72.145"}
      # Updating user_info
      :ok = update_user_info("Jack", 14200, "On", ["Tony", "John"])
      assert fetch_user_info_from_file("Jack") === %{"ip_address" => "231.14.72.145", "port" => 14200, "status" => "On", "allowed_users" => ["Tony", "John"]}
      # De-registering a user
      :ok = deregister_user("Jack")
      assert fetch_user_info_from_file("Jack") === nil
      # Clearing the registry
      clear()
      assert fetch_registry_from_file === %{}
    end
  end
  
  defp create_mock_backup_file do
    json_data = """
    {
      "Tony": {
        "status": "On",
        "port": 15300,
        "allowed_users": [
          "Bob",
          "Bill"
        ],
        "ip_address": "213.143.75.162"
      },
      "John": {
        "status": "Off",
        "port": 8400,
        "allowed_users": [
          "Tony",
          "Mike"
        ],
        "ip_address": "121.19.215.92"
      },
      "Bob": {
        "ip_address": "17.127.82.11"
      }
    }
    """
    File.write!(@registry_filename, json_data)
  end

  defp fetch_user_info_from_file(username) do
    file_registry = fetch_registry_from_file
    Map.get(file_registry, username)
  end

  defp fetch_registry_from_file do
    json = File.read!(@registry_filename)
    Poison.Parser.parse!(json)
  end
end