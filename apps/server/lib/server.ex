defmodule Server do
  @moduledoc """
  A UDP server processing incoming messages and replying to clients.
  """
  use Application

  import Server.MessageProcessor
  import Core.{MessageDecoder, MessageEncoder}

  alias Core.UDP

  require Logger

  @doc false
  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      worker(Server.UserRegistry, []),
      worker(Server.Config, []),
      supervisor(Task.Supervisor, [[name: Server.TaskSupervisor]]),
      worker(UDP, []),
      worker(Task, [Server, :listen, []]),
      worker(Task, [Server.CLI, :accept_commands, []], id: Server.CLI),
    ]

    opts = [strategy: :one_for_one, name: Server.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @doc """
  Opens a socket and starts listening for incoming messages.
  """
  @spec listen() :: no_return
  def listen() do
    {:ok, active_port} = UDP.get_active_port()
    # Start listening
    Logger.info("Listening on port #{active_port}")
    loop_listener()
  end

  @spec loop_listener() :: no_return
  defp loop_listener() do
    case UDP.receive() do
      {:ok, packet} ->
        {:ok, _pid} = Task.Supervisor.start_child(Server.TaskSupervisor, fn -> handle_packet(packet) end)
      {:error, reason} -> 
        Logger.error("UDP error while listening for messages: #{reason}")
    end
    loop_listener()
  end

  @spec handle_packet(UDP.packet) :: :ok
  defp handle_packet({address, port, message}) do
    Logger.info("Received #{inspect message} from {#{inspect address}, #{port}}")
  
    status = 
      with sender_info <- {address, port},
          {:ok, decoded_message} <- decode(sender_info, message),
          {:ok, response_message} <- process(decoded_message),
          {:ok, response} <- encode(response_message),
          :ok <- UDP.send_message(response, {address, port}),
          do: {:ok, response_message}

    case status do
      {:ok, response}  -> Logger.info("Sent response #{inspect response} to {#{inspect address}, #{port}}")
      {:error, reason} -> Logger.error("Error while handling packer: #{reason}")
    end
  end

end
