defmodule Server.MessageProcessor do
  @moduledoc """
  A message processor processing messages by changing user registry state and generating responses.
  """
  import Core.Utilities, only: [parse_ip_string: 1]

  alias Server.UserRegistry, as: Registry
  alias Server.Config
  alias Core.MessageFactory
  alias Core.Messages.{RegisterMessage, DeregisterMessage, PublishMessage, InformReqMessage, FindReqMessage}

  @doc """
  Processes `message` based on its type and returns a response.
  """
  @spec process(struct) :: {:ok, struct}
  def process(%RegisterMessage{} = message) do
    case Registry.register_user(message.username, message.ip_address) do
      :ok -> {:ok, MessageFactory.build_registered(message.request_number)}
      :already_registered -> {:ok, MessageFactory.build_already_registered(message.request_number)}
      :server_full -> 
        {next_server_ip, next_server_port} = Config.get_next_server_address()
        {:ok, MessageFactory.build_register_denied(message.request_number, next_server_ip, next_server_port)}
    end
  end

  def process(%DeregisterMessage{} = message) do
    case Registry.deregister_user(message.username) do
      :ok -> {:ok, MessageFactory.build_deregistered(message.request_number)}
      :user_not_found -> {:ok, MessageFactory.build_user_not_registered(message.request_number, message.username)}
    end
  end

  def process(%PublishMessage{} = message) do
    case Registry.update_user_info(message.username, message.port, message.status, message.allowed_users) do
      :ok -> 
        {:ok, MessageFactory.build_published(message.request_number, message.username, message.port, 
                                             message.status, message.allowed_users)}
      :user_not_found -> 
        {:ok, MessageFactory.build_user_not_registered(message.request_number, message.username)}
    end
  end

  def process(%InformReqMessage{} = message) do
    case Registry.get_user_info(message.username) do
      :user_not_found ->
        {:ok, MessageFactory.build_user_not_registered(message.request_number, message.username)}
      info ->
        if info["status"] === nil do
          {:ok, MessageFactory.build_unpublished(message.request_number)}
        else
          {:ok, MessageFactory.build_inform_resp(message.request_number, message.username, info["port"], 
                                                 info["status"], info["allowed_users"])}
        end 
    end
  end

  def process(%FindReqMessage{} = message) do
    case Registry.get_user_info(message.username) do
      :user_not_found ->
        {next_server_ip, next_server_port} = Config.get_next_server_address()
        {:ok, MessageFactory.build_refer(message.request_number, next_server_ip, next_server_port)}
      info ->
        if info["status"] === nil do
          {:ok, MessageFactory.build_unpublished(message.request_number)}
        else
          build_find_response(message, info)
        end
    end
  end

  @spec build_find_response(%FindReqMessage{}, map) :: {:ok, struct}
  defp build_find_response(message, info) do
    if message.sender_username in info["allowed_users"] do
       if info["status"] === "on" do
          {:ok, ip_address} = parse_ip_string(info["ip_address"])
          {:ok, MessageFactory.build_find_resp(message.request_number, message.username, info["port"], ip_address)
                                                 }
        else
          {:ok, MessageFactory.build_find_resp(message.request_number, message.username, info["status"])}
        end
      else
        {:ok, MessageFactory.build_find_denied(message.request_number, message.username)}
    end
  end

end
