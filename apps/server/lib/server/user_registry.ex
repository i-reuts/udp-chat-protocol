defmodule Server.UserRegistry do
  @moduledoc """
  User registry providing an API to in-memory user registration storage with persistent back-up capabilities.
  """
  alias Poison.Parser
  alias Core.Utilities

  require Logger

  @registry_filename Application.get_env(:server, :registry_filename)
  @max_users Application.get_env(:server, :max_users)

  @doc """
  Starts a link to the reqistry process.

  Initializes the registry from back-up storage if available.
  Returns `{:ok, pid}` on success.
  """
  @spec start_link() :: Agent.on_start()
  def start_link do
    Agent.start_link(fn -> initialize_registry end, name: __MODULE__)
  end

  @doc """
  Similar to start_link/1 but without links (outside of a supervision tree)
  """
  @spec start :: Agent.on_start()
  def start do
    Agent.start(fn -> initialize_registry end, name: __MODULE__)
  end

  @doc """
  Adds a user to the registry and updates back-up storage.
  """
  @spec register_user(String.t, :inet.ip_address) :: :ok | :already_registered | :server_full
  def register_user(username, ip_address) do
    if user_registered?(username) do
      :already_registered
    else
      Logger.info("Current users: #{get_num_registered_users()}, max: #{@max_users}")
      if get_num_registered_users() >= @max_users do
        :server_full
      else
        ip_string = Utilities.ip_to_string(ip_address)
        Agent.update(__MODULE__, &Map.put(&1, username, %{"ip_address" => ip_string}))
        backup_registry()
        :ok
      end
    end
  end

  @doc """
  Removes a user from the registry and updates back-up storage.
  """
  @spec deregister_user(String.t) :: :ok | :user_not_found
  def deregister_user(username) do
    if user_registered?(username) do
      Agent.update(__MODULE__, &Map.delete(&1, username))
      backup_registry()
      :ok
    else
      Logger.warn("Trying to de-register a non-registered user #{username}")
      :user_not_found
    end
  end

  @doc """
  Returns whether a user with `username` is already registered.
  """
  @spec user_registered?(String.t) :: boolean 
  def user_registered?(username) do
    Agent.get(__MODULE__, fn registry -> Map.has_key?(registry, username) end)
  end

  @doc """
  Returns published info about user with `username` or `nil` if user is not registered.
  """
  @spec get_user_info(String.t) :: map | :user_not_found
  def get_user_info(username) do
    case Agent.get(__MODULE__, fn registry -> Map.get(registry, username) end) do
      nil -> :user_not_found
      info -> info
    end
  end

  @doc """
  Updates published info of user with `username`.
  """
  @spec update_user_info(String.t, :inet.port_number, String.t, list) :: :ok | :user_not_found
  def update_user_info(username, port, status, allowed_users) do
    if user_registered?(username) do
      Agent.update(__MODULE__, &update_info(&1, username, port, status, allowed_users))
      backup_registry()
      :ok
    else
      Logger.warn("Trying to update info for a non-registered user #{username}")
      :user_not_found
    end
  end

  @doc """
  Returns the number of users currently registered.
  """
  @spec get_num_registered_users() :: integer
  def get_num_registered_users do 
    Agent.get(__MODULE__, &Enum.count(&1))
  end

  @doc """
  Clears the registry of all registrations and updates back-up storage.
  """
  @spec clear() :: :ok | no_return
  def clear do
    Agent.update(__MODULE__, fn _registry -> %{} end)
    backup_registry()
  end

  @spec initialize_registry() :: map
  defp initialize_registry do
    case File.read(@registry_filename) do
      {:ok, content} ->
        Parser.parse!(content)
      _ ->
        %{}
    end
  end

  @spec backup_registry() :: :ok | no_return
  defp backup_registry do
    registry = Agent.get(__MODULE__, fn registry -> registry end)
    json_data = Poison.encode!(registry, pretty: 1)
    File.write!(@registry_filename, json_data)
  end

  @spec update_info(map, String.t, :inet.port_number, String.t, list) :: map | no_return
  defp update_info(registrations, username, port, status, allowed_users) do
    Map.update!(registrations, username, 
      &Map.merge(&1, %{"port" => port, "status" => status, "allowed_users" => allowed_users}))
  end

end
