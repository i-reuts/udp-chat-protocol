defmodule Server.CLI do
  @moduledoc """
  Command line interface to the chat Server.
  """
  alias Server.Config
  alias Core.Utilities
  
  @doc """
  Initiates command processing loop.
  """
  @spec accept_commands() :: no_return
  def accept_commands do
    read_command |> parse |> execute |> IO.puts
    accept_commands()
  end

  @spec read_command() :: IO.chardata | IO.nodata
  defp read_command do
    IO.gets ""
  end

  @spec parse(String.t) :: tuple | atom
  defp parse(line) do
    case String.split(line) do
      ["SET", "NEXT", "SERVER", ip_string, port_string] -> {:set_next_server, ip_string, port_string}
      ["GET", "NEXT", "SERVER"] -> :get_next_server
      ["EXIT"]     -> :exit
      _            -> :unknown_command
    end
  end

  @spec execute(tuple | atom) :: String.t | tuple | atom
  defp execute({:set_next_server, ip_string, port_string}) do
    result = 
      with {:ok, ip_address}  <- Utilities.parse_ip_string(ip_string),
           {:ok, port_number} <- Utilities.parse_port_number(port_string), 
      do: {:ok, ip_address, port_number}
    
    case result do
      {:ok, ip_address, port_number} ->
        Config.set_next_server_address({ip_address, port_number})
        "Next server set"
      {:error, reason} -> reason
    end
  end

  defp execute(:get_next_server) do
    {ip, port} = Config.get_next_server_address()
    "IP: #{inspect ip}, port: #{port}"
  end

  defp execute(:exit) do
    IO.puts "Exiting..."
    System.halt(0)
  end

  defp execute(:unknown_command) do
    "Command not recognized"
  end

end
