defmodule Server.Config do
  @moduledoc """
  A server configuration store.
  """
  alias Core.UDP
  alias Server.Config

  defstruct next_server_address: {_ip = {0,0,0,0}, _port = 0}

  @doc """
  Starts the configuration store.
  """
  @spec start_link() :: Agent.on_start()
  def start_link do
    Agent.start_link(fn -> %Config{} end, name: __MODULE__)
  end

  @doc """
  Returns address of the next server in the server chain.
  """
  @spec get_next_server_address() :: UDP.address | {nil, nil}
  def get_next_server_address do
    Agent.get(__MODULE__, &Map.get(&1, :next_server_address))
  end

  @doc """
  Sets address of the next server in the server chain.
  """
  @spec set_next_server_address(UDP.address) :: :ok
  def set_next_server_address(server_address) do
    Agent.update(__MODULE__, &Map.put(&1, :next_server_address, server_address))
  end

end
