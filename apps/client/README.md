# Client

Client-side application for UDP Chat Protocol.

## Usage

To run the client, simply execute `mix run --no-halt` from the application directory.

Supported commands:

  * `REGISTER`
  * `DEREGISTER`
  * `PUBLISH`
  * `REQUEST INFO`
  * `FIND <username>`
  * `CHAT <username> <message>`
  * `BYE <username>`
  * `EXIT`

  In order to be able to chat with a user, client must either succesfully FIND the user or receive message from them.

  Default port and response timeout can be changed in `config\config.exs` file.
