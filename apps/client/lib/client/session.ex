defmodule Client.Session do
  @moduledoc """
  A current session store.
  """
  alias Core.UDP

  defstruct username: nil, server_address: {_ip = nil, _port = nil}, request_number: nil, user_list: %{}

  @doc """
  Starts the session store.
  """
  @spec start_link() :: Agent.on_start()
  def start_link do
    Agent.start_link(fn -> %Client.Session{} end, name: __MODULE__)
  end

  @doc """
  Creates new session.
  """
  @spec new(String.t, UDP.address) :: :ok
  def new(username, server_address) do
    Agent.update(__MODULE__, fn _session -> %Client.Session{
      username: username, 
      server_address: server_address, request_number: 0} 
    end)
  end

  @doc """
  Returns current user name.
  """
  @spec get_username() :: String.t
  def get_username do
    Agent.get(__MODULE__, &Map.get(&1, :username))
  end

  @doc """
  Returns current server address `{ip, port}`.
  """
  @spec get_server_address() :: UDP.address
  def get_server_address do
    Agent.get(__MODULE__, &Map.get(&1, :server_address))
  end

  @doc """
  Sets current user name.
  """
  @spec set_username(String.t) :: :ok
  def set_username(username) do
    Agent.update(__MODULE__, &Map.put(&1, :username, username))
  end

  @doc """
  Sets current server address.
  """
  @spec set_server_address(UDP.address) :: :ok
  def set_server_address(value) do
    Agent.update(__MODULE__, &Map.put(&1, :server_address, value))
  end

  @doc """
  Returns request number for the next request.
  """
  @spec get_request_number() :: byte
  def get_request_number do
    current_rq_number = request_number()
    next_rq_number = next_request_number(current_rq_number)

    Agent.update(__MODULE__, &Map.put(&1, :request_number, next_rq_number))
    current_rq_number
  end

  @doc """
  Adds a user entry to the list of known users.
  """
  @spec add_known_user(String.t, UDP.address) :: :ok
  def add_known_user(username, address) do
    Agent.update(__MODULE__, fn session ->
      new_list = Map.put(session.user_list, username, address)
      Map.put(session, :user_list, new_list)
      end)
  end

  @doc """
  Gets a user entry from the list of known users.
  """
  @spec get_known_user_address(String.t) :: UDP.address | nil
  def get_known_user_address(username) do
    Agent.get(__MODULE__, fn session ->
      Map.get(session.user_list, username)
      end)
  end

  @doc """
  Removes a user entry to the list of known users.
  """
  @spec remove_known_user(String.t) :: :ok
  def remove_known_user(username) do
    Agent.update(__MODULE__, fn session ->
      new_list = Map.delete(session.user_list, username)
      Map.put(session, :user_list, new_list)
      end)
  end

  defp request_number do
    Agent.get(__MODULE__, &Map.get(&1, :request_number))
  end

  defp next_request_number(current_rq_number) when current_rq_number >= 255, do: 0
  defp next_request_number(current_rq_number), do: current_rq_number + 1

end
