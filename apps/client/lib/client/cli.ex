defmodule Client.CLI do
  @moduledoc """
  Command line interface to the chat Client.
  """
  import Core.Utilities, only: [validate_username: 1, validate_user_list: 1, parse_ip_string: 1, parse_port_number: 1]

  @doc """
  Reads a command from CLI and parses it, 
  returning an internal representation of the command.
  """
  @spec parse_command() :: atom
  def parse_command do
    read_command |> parse
  end

  @doc """
  Prompts for username and validates the username received.
  """
  @spec get_username() :: String.t
  def get_username do
    username = "Username: " |> IO.gets |> String.trim

    case validate_username(username) do
      :ok -> username
      {:error, message} -> 
        IO.puts(message)
        get_username()
    end
  end

  @doc """
  Prompts for server IP address and port, validates received input.

  All messages are sent to the server specified while current session is active.
  Returns `{ip, port}` tuple.
  """
  @spec get_server_address() :: {:inet.ip_address, :inet.port_number}
  def get_server_address do
    ip = get_server_ip()
    port = get_server_port()
    {ip, port}
  end

  @doc """
  Prints a message to the user.
  """
  @spec write_line(String.t) :: :ok
  def write_line(line) do
    IO.puts(line)
  end

  @spec read_command() :: IO.chardata | IO.nodata
  defp read_command do
    IO.gets "> "
  end

  @spec parse(String.t) :: atom | tuple
  defp parse(line) do
    case String.split(line) do
      ["REGISTER"] -> :register
      ["DEREGISTER"] -> :deregister
      ["PUBLISH"] -> get_publish_info()
      ["REQUEST", "INFO"] -> :inform
      ["FIND", username] -> {:find, username}
      ["CHAT", username | text] -> {:chat, username, Enum.join(text, " ")}
      ["BYE", username] -> {:bye, username}
      ["EXIT"] -> :exit
      _ -> :unknown_command
    end
  end

  @spec get_server_ip() :: :inet.ip_address
  defp get_server_ip do
    ip_string = "Please enter IP address of a chat server: "
      |> IO.gets
      |> String.trim

    case parse_ip_string(ip_string) do
      {:ok, ip}    -> ip
      {:error, reason} ->
        IO.puts "#{reason}. Please try again."
        get_server_ip()
    end
  end

  @spec get_server_port() :: :inet.port_number
  defp get_server_port do
    port_string = IO.gets("Please enter port number of a chat server: ") 

    case parse_port_number(port_string) do
      {:ok, port} ->  port
      {:error, reason} -> 
        IO.puts("#{reason}. Please try again.")
        get_server_port() 
    end
  end

  @spec get_publish_info() :: {:publish, String.t, list}
  defp get_publish_info do
    status = get_status()
    user_list = get_user_list()
    {:publish, status, user_list}
  end

  @spec get_status() :: String.t
  defp get_status do
    status = "Status: " |> IO.gets |> String.trim

    if status === "on" or status === "off" do
      status 
    else
      IO.puts "Invalid status, try again"
      get_status()
    end
  end
  
  @spec get_user_list() :: list
  def get_user_list do
    user_list = "User list: " |> IO.gets |> String.trim |> String.split

    case validate_user_list(user_list) do
      :ok -> user_list
      {:error, reason} -> 
        IO.puts reason
        get_user_list()
    end
  end

end
