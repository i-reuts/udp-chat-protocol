defmodule Client.ChatListener do
  @moduledoc """
  A chat message listener.
  """
  alias Client.{Session, CLI}
  alias Core.{UDP, MessageDecoder}
  alias Core.Messages.{ChatMessage, ByeMessage}

  @doc """
  Listens to chat messages on a separate port
  """
  @spec listen_to_chat_messages() :: no_return
  def listen_to_chat_messages do
    case UDP.receive(:infinity, ChatUDP) do
      {:ok, packet} -> process_chat_packet(packet)
      {:error, reason} -> reason
    end
    listen_to_chat_messages()
  end

  @spec process_chat_packet(UDP.packet) :: :ok
  defp process_chat_packet(packet) do
    {ip, port, message} = packet
    case MessageDecoder.decode({ip, port}, message) do
      {:ok, %ChatMessage{} = message} -> 
        CLI.write_line("Message from #{message.username}: #{message.text}")
        Session.add_known_user(message.username, {message.ip_address, message.port})
      {:ok, %ByeMessage{} = message} ->
        Session.remove_known_user(message.username)
        CLI.write_line("#{message.username} says Bye")
      _ -> CLI.write_line("Invalid message on chat port")
    end
  end

end
