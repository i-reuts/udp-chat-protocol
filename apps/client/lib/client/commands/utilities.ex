defmodule Client.Commands.Utilities  do
  @moduledoc """
  Utility functions used by multiple commands.
  """ 
  alias Client.{Session}
  alias Core.UDP

  @timeout Application.get_env(:client, :default_timeout)
  
  @doc """
  Sends packet to the current server.
  """
  @spec send_packet(binary) :: :ok | {:error, String.t}
  def send_packet(message) do
    UDP.send_message(message, Session.get_server_address())
  end

  @doc """
  Waits for response until timeouts. 
  
  if received, returns response. Otherwise returns timeout error.

  Default timeout can be changed in `config\config.exs` file.
  """
  @spec wait_for_response() :: {:ok, {:inet.ip_address, :inet.port_number, binary}} | {:error, String.t}
  def wait_for_response() do
    UDP.receive(@timeout)
  end

end
