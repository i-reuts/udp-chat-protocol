defmodule Client.Commands.InformCommand do
  @moduledoc """
  Information command handler.
  """
  import Client.Commands.Utilities

  alias Client.{Session}
  alias Core.{MessageEncoder, MessageDecoder, UDP}
  alias Core.Messages.{InformReqMessage, InformRespMessage, UserNotRegisteredMessage, UnpublishedMessage}

  require Logger

  @doc """
  Runs the command by building an inform req message, sending it and processing response.
  """
  @spec execute() :: String.t
  def execute() do
    case _execute do
      {:ok, info} -> info
      :user_not_registered -> "User with such username is not registered"
      :user_not_published -> "This user has not published their info"
      {:error, reason} -> reason
    end
  end

  @spec _execute() :: {:ok, String.t} | :user_not_registered | :user_not_published | {:error, String.t}
  defp _execute do
    with {:ok, message} <- build_message(),
         :ok            <- send_packet(message),
         {:ok, packet}  <- wait_for_response(),
         {:ok, info}    <- process_response(packet),
         do: {:ok, info}
  end

  @spec build_message() :: {:ok, binary} | {:error, String.t}
  defp build_message do
    MessageEncoder.encode(%InformReqMessage{
      request_number: Session.get_request_number(),
      username: Session.get_username()
      })
  end

  @spec process_response(UDP.packet) :: {:ok, String.t} | :user_not_registered 
      | :user_not_published | {:error, String.t}
  defp process_response(packet) do
    {ip, port, message} = packet
    case MessageDecoder.decode({ip, port}, message) do
      {:ok, %InformRespMessage{} = message} -> 
        {:ok, build_info_message(message.username, message.port, message.status, message.allowed_users)}
      {:ok, %UnpublishedMessage{}} -> :user_not_published
      {:ok, %UserNotRegisteredMessage{}} -> :user_not_registered
      {:error, _reason} -> {:error, "Parsing information request acknowledgement failed"}
    end
  end

  defp build_info_message(username, port, status, user_list) do
    "Your published info - username: #{username}, port: #{port}, status: #{status}, allowed users: #{inspect user_list}"
  end

end
