defmodule Client.Commands.FindCommand do
  @moduledoc """
  Find command handler.
  """
  import Client.Commands.Utilities
  import Core.Utilities, only: [validate_username: 1]

  alias Client.{Session}
  alias Core.{MessageEncoder, MessageDecoder, UDP}
  alias Core.Messages.{FindReqMessage, FindRespMessage, FindDeniedMessage, ReferMessage, UnpublishedMessage}

  require Logger

  @doc """
  Runs the command by building a find req message, sending it and processing response.
  """
  @spec execute(String.t) :: String.t
  def execute(username) do
    case validate_username(username) do
      :ok ->
        case _execute(username) do
          {:ok, info} -> info
          :find_denied -> "User does not have you in their allowed user list"
          :user_not_published -> "This user has not published their info"
          {:error, reason} -> reason
        end
      {:error, reason} -> reason
    end
  end

  @spec _execute(String.t, list) :: :ok | :already_registered | :user_not_published | {:error, String.t}
  defp _execute(username, servers_attempted \\ []) do
    case find(username) do
      {:refer, next_server_address} ->
        Logger.info("User not found on the current server.")
        if Enum.member?(servers_attempted, next_server_address) do
          Session.set_server_address(next_server_address)
          {:error, "User not found"}
        else
          servers_attempted = servers_attempted ++ [Session.get_server_address()]
          Session.set_server_address(next_server_address)      
          Logger.info("Re-attemping find with next server #{inspect next_server_address}")
          _execute(username, servers_attempted)
        end
      {:error, reason} -> 
        Session.set_server_address(List.first(servers_attempted))
        Logger.error("Error: #{inspect reason}")
        {:error, "Next server in chain is unavailable"}
      other -> other
    end
  end

  @spec find(String.t) :: {:ok, String.t} | :find_denied | 
      :user_not_published | {:refer, UDP.address} | {:error, String.t}
  defp find(username) do
    with {:ok, message} <- build_message(username),
         :ok            <- send_packet(message),
         {:ok, packet}  <- wait_for_response(),
         {:ok, info}    <- process_response(packet),
         do: {:ok, info}
  end

  @spec build_message(String.t) :: {:ok, binary} | {:error, String.t}
  defp build_message(username) do
    MessageEncoder.encode(%FindReqMessage{
      request_number: Session.get_request_number(),
      sender_username: Session.get_username(),
      username: username
      })
  end

  @spec process_response(UDP.packet) :: {:ok, String.t} | :find_denied | 
        :user_not_published | {:refer, UDP.address} | {:error, String.t}
  defp process_response(packet) do
    {ip, port, message} = packet
    case MessageDecoder.decode({ip, port}, message) do
      {:ok, %FindRespMessage{} = message} -> {:ok, process_info_message(message)}
      {:ok, %FindDeniedMessage{}} -> :find_denied
      {:ok, %UnpublishedMessage{}} -> :user_not_published
      {:ok, %ReferMessage{} = message} -> {:refer, {message.ip_address, message.port}}
      {:error, _reason} -> {:error, "Parsing registration acknowledgement failed"}
    end
  end

  @spec process_info_message(%FindRespMessage{}) :: String.t
  defp process_info_message(%FindRespMessage{status: nil} = message) do
    Session.add_known_user(message.username, {message.ip_address, message.port})
    "User found with IP: #{inspect message.ip_address}, port #{message.port}"
  end
  defp process_info_message(%FindRespMessage{status: "off"}) do
    "User is currently offline"
  end
  defp process_info_message(%FindRespMessage{}) do
    "Invalid FindResp message received"
  end

end
