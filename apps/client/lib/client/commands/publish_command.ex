defmodule Client.Commands.PublishCommand do
  @moduledoc """
  Publish command handler.
  """
  import Client.Commands.Utilities

  alias Client.{Session}
  alias Core.{MessageEncoder, MessageDecoder, UDP}
  alias Core.Messages.{PublishMessage, PublishedMessage, UserNotRegisteredMessage}

  require Logger

  @doc """
  Runs the command by building a publish message, sending it and processing response.
  """
  @spec execute(String.t, list) :: String.t
  def execute(status, user_list) do
    case _execute(status, user_list) do
      :ok -> "Information published"
      :user_not_registered -> "User with such username is not registered"
      {:error, reason} -> reason
    end
  end

  @spec _execute(String.t, list) :: :ok | :user_not_registered | {:error, String.t}
  defp _execute(status, user_list) do
    with {:ok, message} <- build_message(status, user_list),
         :ok            <- send_packet(message),
         {:ok, packet}  <- wait_for_response(),
         :ok            <- process_response(packet),
         do: :ok
  end

  @spec build_message(String.t, list) :: {:ok, binary} | {:error, String.t}
  defp build_message(status, user_list) do
    {:ok, port} = UDP.get_active_port(ChatUDP)

    MessageEncoder.encode(%PublishMessage{
      request_number: Session.get_request_number(),
      username: Session.get_username(),
      port: port, 
      status: status,
      allowed_users: user_list
      })
  end

  @spec process_response(UDP.packet) :: :ok | :user_not_registered | {:error, String.t}
  defp process_response(packet) do
    {ip, port, message} = packet
    case MessageDecoder.decode({ip, port}, message) do
      {:ok, %PublishedMessage{}} -> :ok
      {:ok, %UserNotRegisteredMessage{}} -> :user_not_registered
      {:error, _reason} -> {:error, "Parsing publish acknowledgement failed"}
    end
  end

end
