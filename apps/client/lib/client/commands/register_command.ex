defmodule Client.Commands.RegisterCommand do
  @moduledoc """
  Register command handler.
  """
  import Client.Commands.Utilities

  alias Client.{Session}
  alias Core.{MessageEncoder, MessageDecoder, UDP}
  alias Core.Messages.{RegisterMessage, RegisteredMessage, AlreadyRegisteredMessage, RegisterDeniedMessage}

  require Logger

  @doc """
  Runs the command by building a register message, sending it and processing response.
  """
  @spec execute() :: String.t
  def execute do
    case _execute do
      :ok -> "Registration successful"
      :already_registered -> "User with such username is already registered"
      {:error, reason} -> reason
    end
  end

  @spec _execute(list) :: :ok | :already_registered | {:error, String.t}
  defp _execute(servers_attempted \\ []) do
    case register do
      {:register_denied, next_server_address} ->
        Logger.info("Registration Denied")
        if Enum.member?(servers_attempted, next_server_address) do
          Session.set_server_address(next_server_address)
          {:error, "All servers are full"}
        else
          servers_attempted = servers_attempted ++ [Session.get_server_address()]
          Session.set_server_address(next_server_address)      
          Logger.info("Re-attemping registation with next server #{inspect next_server_address}")
          _execute(servers_attempted)
        end
      {:error, reason} -> 
        Session.set_server_address(List.first(servers_attempted))
        Logger.error("Error: #{inspect reason}")
        {:error, "Next server in chain is unavailable"}
      other -> other
    end
  end

  @spec register() :: :ok | :already_registered | {:register_denied, UDP.address} | {:error, String.t}
  defp register do
    with {:ok, message} <- build_message(),
         :ok            <- send_packet(message),
         {:ok, packet}  <- wait_for_response(),
         :ok            <- process_response(packet),
         do: :ok
  end

  @spec build_message() :: {:ok, binary} | {:error, String.t}
  defp build_message do
    MessageEncoder.encode(%RegisterMessage{
      username: Session.get_username(), 
      request_number: Session.get_request_number()
      })
  end

  @spec process_response(UDP.packet) :: :ok | :already_registered | {:register_denied, UDP.address} 
                                        | {:error, String.t}
  defp process_response(packet) do
    {ip, port, message} = packet
    case MessageDecoder.decode({ip, port}, message) do
      {:ok, %RegisteredMessage{}} -> :ok
      {:ok, %AlreadyRegisteredMessage{}} -> :already_registered
      {:ok, %RegisterDeniedMessage{} = message} -> {:register_denied, {message.ip_address, message.port}}
      {:error, _reason} -> {:error, "Parsing registration acknowledgement failed"}
    end
  end

end
