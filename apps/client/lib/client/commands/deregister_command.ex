defmodule Client.Commands.DeregisterCommand do
  @moduledoc """
  Deregister command handler.
  """
  import Client.Commands.Utilities

  alias Client.{Session}
  alias Core.{MessageEncoder, MessageDecoder, UDP}
  alias Core.Messages.{DeregisterMessage, DeregisteredMessage, UserNotRegisteredMessage}

  require Logger

  @doc """
  Runs the command by building an inform req message, sending it and processing response.
  """
  @spec execute() :: String.t
  def execute() do
    case _execute do
      :ok -> "Succesfully deregistered"
      :user_not_registered -> "User with such username is not registered"
      {:error, reason} -> reason
    end
  end

  @spec _execute() :: :ok | :user_not_registered | {:error, String.t}
  defp _execute do
    with {:ok, message} <- build_message(),
         :ok            <- send_packet(message),
         {:ok, packet}  <- wait_for_response(),
         :ok            <- process_response(packet),
         do: :ok
  end

  @spec build_message() :: {:ok, binary} | {:error, String.t}
  defp build_message do
    MessageEncoder.encode(%DeregisterMessage{
      request_number: Session.get_request_number(),
      username: Session.get_username()
      })
  end

  @spec process_response(UDP.packet) :: :ok | :user_not_registered | {:error, String.t}
  defp process_response(packet) do
    {ip, port, message} = packet
    case MessageDecoder.decode({ip, port}, message) do
      {:ok, %DeregisteredMessage{}} -> :ok
      {:ok, %UserNotRegisteredMessage{}} -> :user_not_registered
      {:error, _reason} -> {:error, "Parsing information request acknowledgement failed"}
    end
  end

end
