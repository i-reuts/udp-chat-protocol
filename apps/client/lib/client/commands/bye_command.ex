defmodule Client.Commands.ByeCommand do
  @moduledoc """
  Bye command handler.
  """
  alias Client.Session
  alias Core.{MessageEncoder, UDP}
  alias Core.Messages.ByeMessage

  require Logger

  @doc """
  Runs the command by building a bye message, sending it and processing response.
  """
  @spec execute(String.t) :: String.t
  def execute(username) do
    case _execute(username) do
      :ok -> "Bye sent"
      {:error, reason} -> reason
    end
  end

  @spec _execute(String.t) :: {:ok, String.t} | :user_not_registered | {:error, String.t}
  defp _execute(username) do
    with {:ok, address} <- get_target_address(username),
         {:ok, message} <- build_message(),
         :ok            <- send_packet(message, address),
         do: :ok
  end

  @spec get_target_address(String.t) :: {:ok, UDP.address} | {:error, String.t}
  defp get_target_address(username) do
    case Session.get_known_user_address(username) do
      nil -> {:error, "Unknown user #{username}"}
      address -> {:ok, address}
    end
  end

  @spec build_message() :: {:ok, binary} | {:error, String.t}
  defp build_message do
    MessageEncoder.encode(%ByeMessage{username: Session.get_username()})
  end

  @spec send_packet(binary, UDP.address) :: :ok | {:error, String.t}
  defp send_packet(message, address) do
     UDP.send_message(message, address, ChatUDP)
  end

end
