defmodule Client.Commands.ChatCommand do
  @moduledoc """
  Chat command handler.
  """
  alias Client.Session
  alias Core.{MessageEncoder, UDP}
  alias Core.Messages.ChatMessage

  require Logger

  @max_text_size Application.get_env(:core, :max_text_size)

  @doc """
  Runs the command by building a chat message, sending it and processing response.
  """
  @spec execute(String.t, String.t) :: String.t
  def execute(username, text) do
    case _execute(username, text) do
      :ok -> "Message sent"
      {:error, reason} -> reason
    end
  end

  @spec _execute(String.t, String.t) :: :ok | {:error, String.t}
  defp _execute(username, text) do
    text_chunks = chunk(text)

    results = Enum.map(text_chunks, fn chunk ->
      result = send_chat_message(username, chunk)
      :timer.sleep(50)
      result
      end)

    if Enum.all?(results, fn r -> r === :ok end) do
      :ok
    else
      Enum.find(results, fn r -> r !== :ok end)
    end
  end

  @spec send_chat_message(String.t, String.t) :: :ok | {:error, String.t}
  defp send_chat_message(username, text) do
    with {:ok, address} <- get_target_address(username),
         {:ok, message} <- build_message(text),
          :ok           <- send_packet(message, address),
          do: :ok
  end

  defp get_target_address(username) do
    case Session.get_known_user_address(username) do
      nil -> {:error, "Unknown user #{username}"}
      address -> {:ok, address}
    end
  end

  @spec build_message(String.t) :: {:ok, binary} | {:error, String.t}
  defp build_message(text) do
    MessageEncoder.encode(%ChatMessage{
      username: Session.get_username(),
      text: text
      })
  end

  @spec send_packet(binary, UDP.address) :: :ok | {:error, String.t}
  defp send_packet(message, address) do
     UDP.send_message(message, address, ChatUDP)
  end

  @spec chunk(String.t) :: list
  defp chunk(text) do
    max_bit_size = 8 * @max_text_size
    do_chunks(text, max_bit_size, [])
  end

  @spec do_chunks(String.t, integer, list) :: list
  defp do_chunks(binary, n, acc) when bit_size(binary) <= n do
    Enum.reverse([binary | acc]) 
  end
  defp do_chunks(binary, n, acc) do
    <<chunk::size(n), rest::bitstring>> = binary
    do_chunks(rest, n, [<<chunk::size(n)>> | acc])
  end

end
