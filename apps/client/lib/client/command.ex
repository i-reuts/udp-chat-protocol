defmodule Client.Command do
  @moduledoc """
  A command facade delegating command execution 
  to their corresponding handlers.
  """
  alias Client.Commands.{RegisterCommand, DeregisterCommand, PublishCommand, InformCommand, FindCommand}
  alias Client.Commands.{ChatCommand, ByeCommand}

  @doc """
  Runs the given command.
  """
  @spec execute(atom) :: String.t | {:error, String.t}
  def execute(command)

  def execute(:register) do
    RegisterCommand.execute()
  end

  def execute(:deregister) do
    DeregisterCommand.execute()
  end

  def execute({:publish, status, user_list}) do
    PublishCommand.execute(status, user_list)
  end

  def execute(:inform) do
    InformCommand.execute()
  end

  def execute({:find, username}) do
    FindCommand.execute(username)
  end

  def execute({:chat, username, text}) do
    ChatCommand.execute(username, text)
  end

  def execute({:bye, username}) do
    ByeCommand.execute(username)
  end

  def execute(:exit) do
    IO.puts "Exiting..."
    System.halt(0)
  end

  def execute(:unknown_command) do
    "Command not recognized"
  end
  
end
