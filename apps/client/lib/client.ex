defmodule Client do
  @moduledoc """
  A UDP chat Client with command line user interface.
  """
  use Application

  alias Core.UDP
  alias Client.{CLI, Session, Command, ChatListener}

  @doc false
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(Client.Session, []),
      worker(Task, [Client, :run, []]),
      worker(UDP, [0]),
      worker(UDP, [0, ChatUDP], id: ChatUDP),
      worker(Task, [ChatListener, :listen_to_chat_messages, []], id: ChatListener)
    ]

    opts = [strategy: :one_for_one, name: KVServer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @doc """
  Starts the application by requesting session info from the user
  and initiating command processing loop.
  """
  @spec run() :: no_return
  def run do
    CLI.write_line("\nStarting the client...\n")
    start_session()

    CLI.write_line("\nSession started. Waiting for commands...\n")
    accept_commands()
  end

  @spec start_session() :: :ok
  defp start_session do
    username = CLI.get_username()
    server_address = CLI.get_server_address()

    Session.new(username, server_address)
  end

  @spec accept_commands() :: no_return
  defp accept_commands do
    CLI.parse_command |> Command.execute |> CLI.write_line
    accept_commands()
  end

end
