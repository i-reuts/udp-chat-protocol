defmodule ChatTest do
  use ExUnit.Case, async: false

  import Core.{MessageDecoder, MessageEncoder}

  alias Client.{Session, Command, ChatListener}
  alias Core.UDP
  alias Core.Messages.{ChatMessage, ByeMessage}

  @max_username_length Application.get_env(:core, :max_username_length)
  @localhost {127,0,0,1}
  @client_port 6378
  @server_port 4031

  # One time setup
  setup_all do
    # Create new session
    Session.start_link()   
    Session.new("test_user", {@localhost, @server_port})
    # Start UDP server
    UDP.start_link(@client_port, ChatUDP)
    # Start chat listener
    {:ok, _pid} = Task.start_link(ChatListener, :listen_to_chat_messages, [])
    :ok
  end
  
  test "chat messages are sent and received" do
    # Add mock user
    Session.add_known_user("mock_user", {@localhost, @server_port}) 
    
    # Start mock server
    validation_message = %ChatMessage{username: "test_user", text: "Hello", ip_address: @localhost, port: @client_port}
    reply = %ChatMessage{username: "mock_user", text: "Hi there"}
    :ok = start_mock_server(validation_message, reply)

    # Run chat command
    assert Command.execute({:chat, "mock_user", "Hello"}) === "Message sent"
  end

  test "bye messages are sent and received" do
    # Add mock user
    Session.add_known_user("mock_user", {@localhost, @server_port}) 

    # Start mock server
    validation_message = %ByeMessage{username: "test_user", ip_address: @localhost, port: @client_port}
    reply = %ByeMessage{username: "mock_user"}
    :ok = start_mock_server(validation_message, reply)

    # Run bye command
    assert Command.execute({:bye, "mock_user"}) === "Bye sent"
  end
  
  test "chat and bye messages return invalid username error when user is not known" do
    Session.new("test_user", {@localhost, @server_port})
    
    assert Command.execute({:chat, "mock_user2", "Hello"}) === "Unknown user mock_user2"
    assert Command.execute({:bye, "mock_user3"}) === "Unknown user mock_user3"
  end

  def start_mock_server(validation_message, reply) do
    # Start new server
    {:ok, pid} = Task.start_link(__MODULE__, :run_mock_server, [validation_message, reply, self(), @server_port])
    # Wait until ready
    receive do
      {:ok, ^pid} -> :ok
    end
  end

  def run_mock_server(validation_message, reply, parent, port) do
    # Open socket
    {:ok, socket} = :gen_udp.open(port, [:binary, active: false])
    send(parent, {:ok, self()})

    # Receive and decode message
    {:ok, packet} = :gen_udp.recv(socket, 0)
    {ip, port, message} = packet
    {:ok, message} = decode({ip, port}, message)

    # Validate message
    assert message === validation_message

    # Encode and send reply
    {:ok, reply} = encode(reply)
    :ok = :gen_udp.send(socket, ip, port, reply)
  end

end
