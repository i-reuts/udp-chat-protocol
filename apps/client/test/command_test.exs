defmodule CommandTest do
  use ExUnit.Case, async: false

  import Core.{MessageDecoder, MessageEncoder}

  alias Client.{Session, Command}
  alias Core.UDP
  alias Core.Messages.{RegisterMessage, RegisteredMessage, AlreadyRegisteredMessage, RegisterDeniedMessage, DeregisterMessage, DeregisteredMessage}
  alias Core.Messages.{PublishMessage, PublishedMessage, UserNotRegisteredMessage, UnpublishedMessage}
  alias Core.Messages.{InformReqMessage, InformRespMessage}
  alias Core.Messages.{FindReqMessage, FindRespMessage, FindDeniedMessage, ReferMessage}

  @max_username_length Application.get_env(:core, :max_username_length)
  @localhost {127,0,0,1}
  @client_port 6379
  @server_port 4041

  # One time setup
  setup_all do
    # Create new session
    Session.start_link()   
    # Start UDP server
    UDP.start_link(@client_port)
    UDP.start_link(0, ChatUDP)
    :ok
  end

  # Clear session before every test
  setup do
    Session.new("test_user", {@localhost, @server_port}) 
  end
  
  describe "register command" do
    test "when successfully registered returns success message" do
      # Build validation message and reply
      validation_message = %RegisterMessage{request_number: 0, username: "test_user", ip_address: @localhost}
      reply = %RegisteredMessage{request_number: 0}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run register command
      assert Command.execute(:register) === "Registration successful"
    end

    test "when receives already_registered returns already registered message" do
      # Build validation message and reply
      validation_message = %RegisterMessage{request_number: 0, username: "test_user", ip_address: @localhost}
      reply = %AlreadyRegisteredMessage{request_number: 0}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run register command
      assert Command.execute(:register) === "User with such username is already registered"
    end
  
    test "when register denied registers with the next free server in chain" do
      # Start busy server 1
      validation_message = %RegisterMessage{request_number: 0, username: "test_user", ip_address: @localhost}
      reply = %RegisterDeniedMessage{request_number: 0, ip_address: @localhost, port: 4042}
      :ok = start_mock_server(validation_message, reply)

      # Start busy server 2
      validation_message = %RegisterMessage{request_number: 1, username: "test_user", ip_address: @localhost}
      reply = %RegisterDeniedMessage{request_number: 1, ip_address: @localhost, port: 4043}
      :ok = start_mock_server(validation_message, reply, 4042)

      # Start free server
      validation_message = %RegisterMessage{request_number: 2, username: "test_user", ip_address: @localhost}
      reply = %RegisteredMessage{request_number: 2}
      :ok = start_mock_server(validation_message, reply, 4043)

      # Run register command
      assert Command.execute(:register)   === "Registration successful"
      assert Session.get_server_address() === {@localhost, 4043}
    end
   
    test "when all servers in chain are full returns servers full message" do
      # Start busy server 1
      validation_message = %RegisterMessage{request_number: 0, username: "test_user", ip_address: @localhost}
      reply = %RegisterDeniedMessage{request_number: 0, ip_address: @localhost, port: 4042}
      :ok = start_mock_server(validation_message, reply)

      # Start busy server 2
      validation_message = %RegisterMessage{request_number: 1, username: "test_user", ip_address: @localhost}
      reply = %RegisterDeniedMessage{request_number: 1, ip_address: @localhost, port: 4043}
      :ok = start_mock_server(validation_message, reply, 4042)

      # Start busy server 3
      validation_message = %RegisterMessage{request_number: 2, username: "test_user", ip_address: @localhost}
      reply = %RegisterDeniedMessage{request_number: 2, ip_address: @localhost, port: 4041}
      :ok = start_mock_server(validation_message, reply, 4043)

      # Run register command
      assert Command.execute(:register)   === "All servers are full"
      assert Session.get_server_address() === {@localhost, 4041}
    end

    test "when next server in chain is unavailable returns server unavailable message" do
      # Set next server to 0
      Session.set_server_address({{0,0,0,0}, 4040})
      # Run register command
      assert Command.execute(:register) === "Next server in chain is unavailable"
    end
  end

  describe "deregister command" do
    test "when successfully deregistered returns deregister message" do
      # Build validation message and reply
      validation_message = %DeregisterMessage{request_number: 0, username: "test_user"}
      reply = %DeregisteredMessage{request_number: 0}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run deregister command
      assert Command.execute(:deregister) === "Succesfully deregistered"
    end
       
    test "when user not registered returns not registered message" do
      # Build validation message and reply
      validation_message = %DeregisterMessage{request_number: 0, username: "test_user"}
      reply = %UserNotRegisteredMessage{request_number: 0, username: "test_user"}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run deregister command
      assert Command.execute(:deregister) === "User with such username is not registered"
    end
  end

  describe "publish command" do
    test "when successfully published returns success message" do
      # Build validation message and reply
      {:ok, active_port} = UDP.get_active_port(ChatUDP)
      validation_message = %PublishMessage{request_number: 0, username: "test_user", status: "on", port: active_port, allowed_users: ["Jane", "Mark"]}
      reply = %PublishedMessage{request_number: 0, username: "test_user", status: "on", port: @client_port, allowed_users: ["Jane", "Mark"]}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run publish command
      assert Command.execute({:publish, "on", ["Jane", "Mark"]}) === "Information published"
    end
       
    test "when user not registered returns not registered message" do
      # Build validation message and reply
      {:ok, active_port} = UDP.get_active_port(ChatUDP)
      validation_message = %PublishMessage{request_number: 0, username: "test_user", status: "on", port: active_port, allowed_users: ["Jane", "Mark"]}
      reply = %UserNotRegisteredMessage{request_number: 0, username: "test_user"}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run publish command
      assert Command.execute({:publish, "on", ["Jane", "Mark"]}) === "User with such username is not registered"
    end
  end

  describe "inform command" do
    test "returns user info" do
      # Build validation message and reply
      validation_message = %InformReqMessage{request_number: 0, username: "test_user"}
      reply = %InformRespMessage{request_number: 0, username: "test_user", status: "on", port: @client_port, allowed_users: ["Jane", "Mark"]}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run inform command
      assert Command.execute(:inform) === ~s(Your published info - username: test_user, port: #{@client_port}, status: on, allowed users: ["Jane", "Mark"])
    end
       
    test "when user not registered returns not registered message" do
      # Build validation message and reply
      validation_message = %InformReqMessage{request_number: 0, username: "test_user"}
      reply = %UserNotRegisteredMessage{request_number: 0, username: "test_user"}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run inform command
      assert Command.execute(:inform) === "User with such username is not registered"
    end

    test "when user not published returns not published message" do
      # Build validation message and reply
      validation_message = %InformReqMessage{request_number: 0, username: "test_user"}
      reply = %UnpublishedMessage{request_number: 0}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run inform command
      assert Command.execute(:inform) === "This user has not published their info"
    end
  end

  describe "find command" do
    test "returns user info if found (online)" do
      # Build validation message and reply
      validation_message = %FindReqMessage{request_number: 0, sender_username: "test_user", username: "test_user1"}
      reply = %FindRespMessage{request_number: 0, username: "test_user1", port: @client_port, ip_address: @localhost}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run find command
      assert Command.execute({:find, "test_user1"}) === "User found with IP: #{inspect @localhost}, port #{@client_port}"
    end

    test "returns user info if found (offline)" do
      # Build validation message and reply
      validation_message = %FindReqMessage{request_number: 0, sender_username: "test_user", username: "test_user1"}
      reply = %FindRespMessage{request_number: 0, username: "test_user1", status: "off"}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run find command
      assert Command.execute({:find, "test_user1"}) === "User is currently offline"
    end

    test "when find denied returns a denied message" do
      # Build validation message and reply
      validation_message = %FindReqMessage{request_number: 0, sender_username: "test_user", username: "test_user1"}
      reply = %FindDeniedMessage{request_number: 0, username: "test_user1"}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run find command
      assert Command.execute({:find, "test_user1"}) === "User does not have you in their allowed user list"
    end

    test "when refered to the next server retries find" do
      # Start server 1
      validation_message = %FindReqMessage{request_number: 0, sender_username: "test_user", username: "test_user1"}
      reply = %ReferMessage{request_number: 0, ip_address: @localhost, port: 4042}
      :ok = start_mock_server(validation_message, reply)

      # Start server 2
      validation_message = %FindReqMessage{request_number: 1, sender_username: "test_user", username: "test_user1"}
      reply = %ReferMessage{request_number: 1, ip_address: @localhost, port: 4043}
      :ok = start_mock_server(validation_message, reply, 4042)

      # Start server with target user
      validation_message = %FindReqMessage{request_number: 2, sender_username: "test_user", username: "test_user1"}
      reply = %FindRespMessage{request_number: 2, username: "test_user1", port: @client_port, ip_address: @localhost}
      :ok = start_mock_server(validation_message, reply, 4043)

      # Run find command
      assert Command.execute({:find, "test_user1"})   === "User found with IP: #{inspect @localhost}, port #{@client_port}"
      assert Session.get_server_address() === {@localhost, 4043}
    end

    test "when user is not registered on any server returns user not registered message" do
      # Start server 1
      validation_message = %FindReqMessage{request_number: 0, sender_username: "test_user", username: "test_user1"}
      reply = %ReferMessage{request_number: 0, ip_address: @localhost, port: 4042}
      :ok = start_mock_server(validation_message, reply)

      # Start server 2
      validation_message = %FindReqMessage{request_number: 1, sender_username: "test_user", username: "test_user1"}
      reply = %ReferMessage{request_number: 1, ip_address: @localhost, port: 4043}
      :ok = start_mock_server(validation_message, reply, 4042)

      # Start server with target user
      validation_message = %FindReqMessage{request_number: 2, sender_username: "test_user", username: "test_user1"}
      reply = %ReferMessage{request_number: 2, ip_address: @localhost, port: 4041}
      :ok = start_mock_server(validation_message, reply, 4043)

      # Run find command
      assert Command.execute({:find, "test_user1"})   === "User not found"
      assert Session.get_server_address() === {@localhost, 4041}
    end

    test "when next server in chain is unavailable returns server unavailable message" do
      # Set next server to 0
      Session.set_server_address({{0,0,0,0}, 4040})
      # Run register command
      assert Command.execute({:find, "test_user1"}) === "Next server in chain is unavailable"
    end

    test "when user has not published returns not published message" do
      # Build validation message and reply
      validation_message = %FindReqMessage{request_number: 0, sender_username: "test_user", username: "test_user1"}
      reply = %UnpublishedMessage{request_number: 0}
      # Start mock server
      :ok = start_mock_server(validation_message, reply)
      # Run find command
      assert Command.execute({:find, "test_user1"}) === "This user has not published their info"
    end

    test "find with long username returns an error message" do
      assert Command.execute({:find, "ThisUsernameIsWayTooLong"}) === "Username cannot be empty or exceed the maximum length of #{@max_username_length}"
    end
  end

  def start_mock_server(validation_message, reply, port \\ @server_port) do
    # Start new server
    {:ok, pid} = Task.start_link(__MODULE__, :run_mock_server, [validation_message, reply, self(), port])
    # Wait until ready
    receive do
      {:ok, ^pid} -> :ok
    end
  end

  def run_mock_server(validation_message, reply, parent, port) do
    # Open socket
    {:ok, socket} = :gen_udp.open(port, [:binary, active: false])
    send(parent, {:ok, self()})

    # Receive and decode message
    {:ok, packet} = :gen_udp.recv(socket, 0)
    {ip, port, message} = packet
    {:ok, message} = decode({ip, port}, message)

    # Validate message
    assert message === validation_message

    # Encode and send reply
    {:ok, reply} = encode(reply)
    :ok = :gen_udp.send(socket, ip, port, reply)
  end

end
