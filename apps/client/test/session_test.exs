defmodule SessionTest do
  use ExUnit.Case

  alias Client.Session

  @username "test_user"
  @server_address {{231,43,19,168}, 9783}

  @new_username "test_user_new"
  @new_server_address {{17,24,127,98}, 15300}

  test "session API operates correctly" do
    # Start session store
    Session.start_link()

    # Create new session
    Session.new(@username, @server_address)
    assert Session.get_username() === @username
    assert Session.get_server_address() === @server_address

    # Set values
    Session.set_username(@new_username)
    assert Session.get_username() === @new_username

    Session.set_server_address(@new_server_address)
    assert Session.get_server_address() === @new_server_address

    # Request number is incremented on each call
    assert Session.get_request_number() === 0
    assert Session.get_request_number() === 1

    # Request number loops back to 0 after 255
    for _ <- 1..254 do Session.get_request_number() end
    assert Session.get_request_number() === 0

    # Add a user to known user list
    Session.add_known_user("Jane", {{127,19,95,51}, 6789})
    assert Session.get_known_user_address("Jane") === {{127,19,95,51}, 6789}

    # Removes a user from known user list
    Session.remove_known_user("Jane")
    assert Session.get_known_user_address("Jane") === nil
  end
end
