# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure for your application as:
#
#     config :core, key: :value
#
# And access this configuration in your application as:
#
#     Application.get_env(:core, :key)
#
# Or configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"

config :core, max_username_length: 12
config :core, max_text_size: 140

config :core, message_headers: %{1   => :register,
                                 2   => :deregister, 
                                 11  => :publish,
                                 21  => :inform_req,
                                 31  => :find_req,

                                 51  => :chat,
                                 52  => :bye,

                                 101 => :registered,
                                 102 => :deregistered,
                                 111 => :published,
                                 121 => :inform_resp,
                                 131 => :find_resp,

                                 201 => :register_denied,
                                 202 => :already_registered,

                                 211 => :unpublished,

                                 231 => :find_denied,
                                 232 => :refer,

                                 251 => :user_not_registered
                                 }