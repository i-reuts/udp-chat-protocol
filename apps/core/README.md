# Core

A library project containing code used by both Server and Client. 
Defines message formats and message coders, as well as several utility functions.