defmodule Core.Utilities do
  @moduledoc """
  A collection of utility functions used in multiple modules.
  """

  @max_username_length Application.get_env(:core, :max_username_length)
  @max_text_size Application.get_env(:core, :max_text_size)
  @message_headers Application.get_env(:core, :message_headers)
   
  @doc """
  Checks if request number is within acceptable bounds.
  """
  @spec validate_request_number(integer) :: :ok | {:error, String.t}
  def validate_request_number(request_number) do
    if request_number >= 0 and request_number < 255 do
      :ok
    else
      {:error, "Request number number #{request_number} is outside the allowed range"}
    end
  end

  @doc """
  Checks if username length is less than the maximum allowed length.

  The maximum length can be adjusted in config.exs file.
  """
  @spec validate_username(String.t) :: :ok | {:error, String.t}
  def validate_username(username) do
    name_length = String.length(username)
    if name_length in 1..@max_username_length do
      :ok
    else
      {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"}
    end
  end

  @doc """
  Checks if every username in the user list is valid.
  """
  @spec validate_user_list(list) :: :ok | {:error, String.t}
  def validate_user_list(user_list) do
    is_valid = Enum.all?(user_list, fn username -> validate_username(username) === :ok end)
    if is_valid do
      :ok
    else
      {:error, "Invalid entry in the user list"}
    end
  end 

  @doc """
  Validates ip_address structure to contain a valid IPv4 address.
  """
  @spec validate_ip_address(:inet.ip_address) :: :ok | {:error, String.t}
  def validate_ip_address(ip_address) do
    ip_address = Tuple.to_list(ip_address)
    is_valid = length(ip_address) === 4 && Enum.all?(ip_address, fn ip -> ip in 0..255 end)
    if is_valid do
      :ok
    else
      {:error, "Invalid IP address"}
    end
  end

  @doc """
  Checks if text size is less than the maximum allowed size.

  The maximum size can be adjusted in config.exs file.
  """
  @spec validate_text(String.t) :: :ok | {:error, String.t}
  def validate_text(text) do
    text_size = byte_size(text)
    if text_size in 1..@max_text_size do
      :ok
    else
      {:error, "Text cannot be empty or exceed the maximum size of #{@max_text_size}"}
    end
  end

  @doc """
  Checks if port_number is valid.
  """
  @spec validate_port_number(:inet.port_number) :: :ok | {:error, String.t}
  def validate_port_number(port) do
    if port in 0..65_535 do
      :ok
    else
      {:error, "Invalid port number"}
    end
  end

  @doc """
  Converts an IP address string to internal ip_address data structure.
  """
  @spec parse_ip_string(String.t) :: {:ok, :inet.ip_address} | {:error, String.t}
  def parse_ip_string(ip_string) do
    case ip_string |> to_charlist |> :inet.parse_address do
      {:ok, ip_address} -> {:ok, ip_address}
      {:error, :einval} -> {:error, "Invalid IP address"}
    end
  end

  @doc """
  Converts an internal ip_address data structure to string.
  """
  @spec ip_to_string(:inet.ip_address) :: String.t
  def ip_to_string(ip_address) do
    ip_address |> :inet.ntoa |> to_string
  end

  @doc """
  Converts a port string to a port number if valid.
  """
  @spec parse_port_number(String.t) :: {:ok, :inet.port_number} | {:error, String.t}
  def parse_port_number(port_string) do
    case Integer.parse(port_string) do
      {port, _remainder} -> 
        case validate_port_number(port) do
          :ok -> {:ok, port}
          {:error, reason} -> {:error, reason}
        end
      :error -> {:error, "Port argument is not a number"}
    end
  end

  @doc """
  Converts status string to an integer representation
  """
  @spec parse_status(String.t) :: {:ok, 0|1} | {:error, String.t}
  def parse_status("on"), do: {:ok, 1}
  def parse_status("off"), do: {:ok, 0}
  def parse_status(_), do: {:error, "User status invalid, assign as either 'on' or 'off'"}

  @doc """
  Converts integer status representation to string.
  """
  @spec integer_to_status(0|1) :: String.t
  def integer_to_status(0), do: "off"
  def integer_to_status(1), do: "on"

  @doc """
  Encodes IP address data structure into a binary.
  """
  @spec encode_ip(:inet.ip_address) :: binary
  def encode_ip(ip_address) do
    {ip1, ip2, ip3, ip4} = ip_address
    <<ip1::size(8), ip2::size(8), ip3::size(8), ip4::size(8)>>
  end

  @doc """
  Decodes IP address binary into an IP address data structure.
  """
  @spec decode_ip(binary) :: :inet.ip_address
  def decode_ip(ip_binary) do
    <<ip1::size(8), ip2::size(8), ip3::size(8), ip4::size(8)>> = ip_binary
    {ip1, ip2, ip3, ip4}
  end

  @doc """
  Returns message type atom corresponsing to the encoded message `header`.

  Header mappings are configured in config.exs file.
  Throws `KeyError` if message header is not in the header map.
  """
  @spec get_message_type(byte) :: atom
  def get_message_type(header) do
    Map.fetch!(@message_headers, header)
  end

  @doc """
  Returns an encoded header corresponding to given `message_type` atom.

  Header mappings are configured in config.exs file.
  Throws `ArgumentError` if message header is not in the header map.
  """
  @spec get_message_header(atom) :: byte
  def get_message_header(message_type) do
    @message_headers
      |> Enum.find(fn {_key, val} -> val == message_type end)
      |> elem(0)  
  end

end
