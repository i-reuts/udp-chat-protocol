defmodule Core.MessageFactory do
  @moduledoc """
  A factory for building message structures.
  """
  alias Core.Messages.{RegisterMessage, RegisteredMessage, AlreadyRegisteredMessage, RegisterDeniedMessage, DeregisterMessage, DeregisteredMessage}
  alias Core.Messages.{PublishMessage, PublishedMessage, UnpublishedMessage}
  alias Core.Messages.{InformReqMessage, InformRespMessage}
  alias Core.Messages.{FindReqMessage, FindRespMessage, FindDeniedMessage, ReferMessage}
  alias Core.Messages.{ChatMessage, ByeMessage}
  alias Core.Messages.{UserNotRegisteredMessage}

  # Register 

  @spec build_register(byte, String.t) :: %RegisterMessage{}
  def build_register(request_number, username) do
    %RegisterMessage{request_number: request_number, username: username}
  end

  @spec build_registered(byte) :: %RegisteredMessage{}
  def build_registered(request_number) do
    %RegisteredMessage{request_number: request_number}
  end

  @spec build_already_registered(byte) :: %AlreadyRegisteredMessage{}
  def build_already_registered(request_number) do
    %AlreadyRegisteredMessage{request_number: request_number}
  end

  @spec build_register_denied(byte, :inet.ip_address, :inet.port_number) :: %RegisterDeniedMessage{}
  def build_register_denied(request_number, ip_address, port) do
    %RegisterDeniedMessage{request_number: request_number, ip_address: ip_address, port: port}
  end

  @spec build_deregister(byte, String.t) :: %DeregisterMessage{}
  def build_deregister(request_number, username) do
    %DeregisterMessage{request_number: request_number, username: username}
  end

  @spec build_deregistered(byte) :: %DeregisteredMessage{}
  def build_deregistered(request_number) do
    %DeregisteredMessage{request_number: request_number}
  end

  # Publish

  @spec build_publish(byte, String.t, :inet.port_number, String.t, list) :: %PublishMessage{}
  def build_publish(request_number, username, port, status, allowed_users) do
    %PublishMessage{request_number: request_number, username: username, port: port, 
                    status: status, allowed_users: allowed_users}
  end

  @spec build_published(byte, String.t, :inet.port_number, String.t, list) :: %PublishedMessage{}
  def build_published(request_number, username, port, status, allowed_users) do
    %PublishedMessage{request_number: request_number, username: username, port: port, 
                      status: status, allowed_users: allowed_users}
  end

  @spec build_unpublished(byte) :: %UnpublishedMessage{}
  def build_unpublished(request_number) do
    %UnpublishedMessage{request_number: request_number}
  end

  # Inform 

  @spec build_inform_req(byte, String.t) :: %InformReqMessage{}
  def build_inform_req(request_number, username) do
    %InformReqMessage{request_number: request_number, username: username}
  end

  @spec build_inform_resp(byte, String.t, :inet.port_number, String.t, list) :: %InformRespMessage{}
  def build_inform_resp(request_number, username, port, status, allowed_users) do
    %InformRespMessage{request_number: request_number, username: username, port: port, 
                       status: status, allowed_users: allowed_users}
  end

  # Find 

  @spec build_find_req(byte, String.t, String.t) :: %FindReqMessage{}
  def build_find_req(request_number, sender_username, username) do
    %FindReqMessage{request_number: request_number, sender_username: sender_username, username: username}
  end

  @spec build_find_resp(byte, String.t, :inet.port_number, :inet.ip_address) :: %FindRespMessage{}
  def build_find_resp(request_number, username, port, ip_address) do
    %FindRespMessage{request_number: request_number, username: username, port: port, ip_address: ip_address}
  end
  @spec build_find_resp(byte, String.t, String.t) :: %FindRespMessage{}
  def build_find_resp(request_number, username, status) do
    %FindRespMessage{request_number: request_number, username: username, status: status}
  end

  @spec build_find_denied(byte, String.t) :: %FindDeniedMessage{}
  def build_find_denied(request_number, username) do
    %FindDeniedMessage{request_number: request_number, username: username}
  end

  @spec build_refer(byte, :inet.ip_address, :inet.port_number) :: %ReferMessage{}
  def build_refer(request_number, ip_address, port) do
    %ReferMessage{request_number: request_number, ip_address: ip_address, port: port}
  end

  # Utility

  @spec build_user_not_registered(byte, String.t) :: %UserNotRegisteredMessage{}
  def build_user_not_registered(request_number, username) do
    %UserNotRegisteredMessage{request_number: request_number, username: username}
  end

  # Chat

  @spec build_chat(String.t, String.t) :: %ChatMessage{}
  def build_chat(username, text) do
    %ChatMessage{username: username, text: text}
  end

  @spec build_bye(String.t) :: %ByeMessage{}
  def build_bye(username) do
    %ByeMessage{username: username}
  end

end
