defmodule Core.MessageDecoder do
  @moduledoc """
  A message decoder facade.
  """
  import Core.Utilities, only: [get_message_type: 1]

  alias Core.UDP
  alias Core.Messages.{RegisterMessage, RegisteredMessage, AlreadyRegisteredMessage, RegisterDeniedMessage, DeregisterMessage, DeregisteredMessage}
  alias Core.Messages.{PublishMessage, PublishedMessage, UnpublishedMessage}
  alias Core.Messages.{InformReqMessage, InformRespMessage}
  alias Core.Messages.{FindReqMessage, FindRespMessage, FindDeniedMessage, ReferMessage}
  alias Core.Messages.{ChatMessage, ByeMessage}
  alias Core.Messages.{UserNotRegisteredMessage}

  require Logger

  @doc """
  Decodes `message` by finding a corresponding message decoder and delegating the actual decoding to it.

  Logs decoding errors and debug information.
  """
  @spec decode(UDP.address, binary) :: {:ok, struct} | {:error, String.t}
  def decode(sender_address, message) do
    Logger.debug("Decoding the message...")
  
    case _decode(sender_address, message) do
      {:ok, decoded_message}  -> 
        Logger.debug("Message decoded: #{inspect decoded_message}")
        {:ok, decoded_message}
      {:error, reason} -> 
        Logger.error("Failed to decode message: #{reason}")
        {:error, reason}
    end
  end

  @spec _decode(UDP.address, binary) :: {:ok, struct} | {:error, String.t}
  defp _decode(sender_address, message) do
    with {:ok, header, body}    <- decode_header(message),
         {:ok, decoded_message} <- decode_body(header, sender_address, body),
         do: {:ok, decoded_message}
  end

  @spec decode_header(binary) :: {:ok, atom, binary} | {:error, String.t}
  defp decode_header(message) do
    try do
      <<header::size(8), body::binary>> = message
      header = get_message_type(header)
      {:ok, header, body} 
    rescue
      _e in MatchError -> {:error, "Invalid message header format - message might be damaged"}
      _e in KeyError   -> {:error, "Unknown message header - message might be damaged"}
    end
  end

  @spec decode_body(atom, UDP.address, binary) :: {:ok, struct()} | {:error, String.t}
  defp decode_body(header, sender_address, body) do
    try do
      _decode_body(header, sender_address, body)
    rescue
      _e in MatchError -> {:error, "Invalid message body format - message might be damaged"}
    end
  end

  @spec _decode_body(atom, UDP.address, binary) :: {:ok, struct()} | {:error, String.t}
  defp _decode_body(:register, sender_address, body) do
    RegisterMessage.decode(sender_address, body)
  end
  defp _decode_body(:registered, _sender_address, body) do
    RegisteredMessage.decode(body)
  end
  defp _decode_body(:already_registered, _sender_address, body) do
    AlreadyRegisteredMessage.decode(body)
  end
  defp _decode_body(:register_denied, _sender_address, body) do
    RegisterDeniedMessage.decode(body)
  end
  defp _decode_body(:deregister, _sender_address, body) do
    DeregisterMessage.decode(body)
  end
  defp _decode_body(:deregistered, _sender_address, body) do
    DeregisteredMessage.decode(body)
  end
  defp _decode_body(:publish, _sender_address, body) do
    PublishMessage.decode(body)
  end
  defp _decode_body(:published, _sender_address, body) do
    PublishedMessage.decode(body)
  end
  defp _decode_body(:unpublished, _sender_address, body) do
    UnpublishedMessage.decode(body)
  end
  defp _decode_body(:inform_req, _sender_address, body) do
    InformReqMessage.decode(body)
  end
  defp _decode_body(:inform_resp, _sender_address, body) do
    InformRespMessage.decode(body)
  end
  defp _decode_body(:find_req, _sender_address, body) do
    FindReqMessage.decode(body)
  end
  defp _decode_body(:find_resp, _sender_address, body) do
    FindRespMessage.decode(body)
  end
  defp _decode_body(:find_denied, _sender_address, body) do
    FindDeniedMessage.decode(body)
  end
  defp _decode_body(:refer, _sender_address, body) do
    ReferMessage.decode(body)
  end
  defp _decode_body(:chat, sender_address, body) do
    ChatMessage.decode(sender_address, body)
  end
  defp _decode_body(:bye, sender_address, body) do
    ByeMessage.decode(sender_address, body)
  end
  defp _decode_body(:user_not_registered, _sender_address, body) do
    UserNotRegisteredMessage.decode(body)
  end
end
