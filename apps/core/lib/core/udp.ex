defmodule Core.UDP do
  @moduledoc """
  A UDP server responsible for sending and receiving messages through UDP.
  """
  use GenServer

  require Logger

  @type address :: {:inet.ip_address, :inet.port_number}
  @type packet  :: {:inet.ip_address, :inet.port_number, binary}

  @initial_state %{socket: nil, port: nil}

  # Client API

  @doc """
  Starts the UDP server on `port`.
  """
  @spec start_link(:inet.port_number, atom) :: Agent.on_start
  def start_link(port \\ 0, name \\ __MODULE__) do
    GenServer.start_link(__MODULE__, port, name: name)
  end

  @doc """
  Sends a `message` to `destination`, where `destination` is `{ip, port}`.
  """
  @spec send_message(binary, address, atom) :: :ok | {:error, String.t}
  def send_message(message, destination, name \\ __MODULE__) do
    case GenServer.call(name, {:send, message, destination}) do
      :ok ->
        Logger.debug("Sent UDP packet to #{inspect destination}") 
        :ok
      {:error, reason} -> {:error, "Failed to send packed: #{reason}"}
    end
  end

  @doc """
  Waits for a UDP message and receives it.
  """
  @spec receive(timeout, atom) :: {:ok, packet} | {:error, :timeout} | {:error, String.t}
  def receive(timeout \\ :infinity, name \\ __MODULE__) do
    case GenServer.call(name, {:receive, timeout}, :infinity) do
      {:ok, packet} ->
        {ip, port, _message} = packet
        Logger.debug("Received UDP packet from {#{inspect ip}, #{port}}")  
        {:ok, packet}
      {:error, :timeout} -> {:error, "Server did not respond before the timeout."}
      {:error, reason} -> {:error, "Error while receiving response: #{reason}."}
    end
  end

  @doc """
  Returns port number of the active socket.
  """ 
  @spec get_active_port(atom) :: {:ok, :inet.port_number}
  def get_active_port(name \\ __MODULE__) do
    GenServer.call(name, {:get_active_port}, :infinity)
  end

  # Internal Implementation

  def init(port) do
    opts = [:binary, active: false]
    {:ok, socket} = :gen_udp.open(port, opts)
    {:ok, %{@initial_state | socket: socket}}
  end

  def handle_call({:send, message, {ip, port}}, _from, %{socket: socket} = state) do
    status = :gen_udp.send(socket, ip, port, message)
    {:reply, status, state}
  end

  def handle_call({:receive, timeout}, from, %{socket: socket} = state) do
    {:ok, _pid} = Task.start_link(fn ->
      result = :gen_udp.recv(socket, 0, timeout)
      GenServer.reply(from, result)
    end)
    {:noreply, state}
  end

  def handle_call({:get_active_port}, _from, %{socket: socket} = state) do
    result = :inet.port(socket)
    {:reply, result, state}
  end

end
