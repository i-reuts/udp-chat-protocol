defmodule Core.Messages.ByeMessage do 
  @moduledoc """
  A struct representing ByeMessage and its associated encoder/decoder.
  """
  import Core.Utilities

  alias Core.UDP
  alias Core.Messages.ByeMessage

  require Logger

  @enforce_keys [:username]
  defstruct [:username, :ip_address, :port]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct) :: {:ok, binary} | {:error | String.t}
  def encode(message) do
    Logger.debug("Encoding a bye message with username: #{message.username}")

    case validate_username(message.username) do
      :ok -> 
        header = get_message_header(:bye)
        name_size = byte_size(message.username)
        message = <<header::size(8), name_size::size(8), message.username::binary-size(name_size)>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end
  
  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(UDP.address, binary) :: {:ok, map}
  def decode(_sender_address = {ip_address, port}, message) do
    <<name_size::size(8), username::binary-size(name_size)>> = message
    {:ok, %ByeMessage{username: username, ip_address: ip_address, port: port}}
  end

end		
