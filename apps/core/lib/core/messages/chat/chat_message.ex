defmodule Core.Messages.ChatMessage do 
  @moduledoc """
  A struct representing ChatMessage and its associated encoder/decoder.
  """
  import Core.Utilities

  alias Core.UDP
  alias Core.Messages.ChatMessage

  require Logger

  @enforce_keys [:username, :text]
  defstruct [:username, :ip_address, :port, :text]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct) :: {:ok, binary} | {:error | String.t}
  def encode(message) do
    Logger.debug("Encoding a chat message with username: #{message.username}, text: #{message.text}")

    case validate_args(message.username, message.text) do
      :ok -> 
        header = get_message_header(:chat)
        name_size = byte_size(message.username)
        message = <<header::size(8), name_size::size(8), message.username::binary-size(name_size), message.text::binary>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end
  
  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(UDP.address, binary) :: {:ok, map}
  def decode(_sender_address = {ip_address, port}, message) do
    <<name_size::size(8), username::binary-size(name_size), text::binary>> = message
    {:ok, %ChatMessage{username: username, ip_address: ip_address, port: port, text: text}}
  end

  @spec validate_args(String.t, String.t) :: {:ok, integer, byte, binary} | {:error, String.t}
  defp validate_args(username, text) do
    with :ok  <- validate_username(username),
         :ok  <- validate_text(text),
         do: :ok
  end

end		
