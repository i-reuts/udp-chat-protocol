defmodule Core.Messages.FormatB do 
  @moduledoc """
  Shared encoders and decoders for messages with body format B.

  Body format B: <<request_number, username>>.
  """
  import Core.Utilities

  require Logger

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct, atom) :: {:ok, binary} | {:error | String.t}
  def encode(message, type) do
    Logger.debug("Encoding #{type} message with RQ#: #{message.request_number}, 
                  username: #{message.username}")

    case validate_args(message.request_number, message.username) do
      :ok -> 
        header = get_message_header(type)
        message = <<header::size(8), message.request_number::size(8), message.username::binary>>
        {:ok, message}
      {:error, reason} -> 
        {:error, reason}
    end
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, map}
  def decode(message) do
    <<request_number::size(8), username::binary>> = message 
    {:ok, %{request_number: request_number, username: username}}
  end

  @spec validate_args(byte, String.t) :: :ok | {:error, String.t}
  defp validate_args(request_number, username) do
    with :ok <- validate_request_number(request_number), 
         :ok <- validate_username(username),    
         do: :ok
  end

end
