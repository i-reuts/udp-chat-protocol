defmodule Core.Messages.FormatD do 
  @moduledoc """
  Shared encoders and decoders for messages with body format D.

  Body format D: <<request_number, ip_address, port>>.
  """
  import Core.Utilities

  require Logger

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct, atom) :: {:ok, binary} | {:error | String.t}
  def encode(message, type) do
    Logger.debug("Encoding #{type} message with RQ#: #{message.request_number}, 
                  IP: #{inspect message.ip_address}, port: #{message.port}")
    
    case validate_args(message.request_number, message.ip_address, message.port) do
      :ok -> 
        header = get_message_header(type)
        ip_binary = encode_ip(message.ip_address)
        message = <<header::size(8), message.request_number::size(8), ip_binary::binary-size(4), message.port::size(16)>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end
  
  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(binary) :: {:ok, map}
  def decode(message) do
    <<request_number::size(8), ip_binary::binary-size(4), port_number::size(16)>> = message
    ip_address = decode_ip(ip_binary)
    {:ok, %{request_number: request_number, ip_address: ip_address, port: port_number}}
  end

  @spec validate_args(byte, :inet.ip_address, :inet.port_number) :: :ok | {:error, String.t}
  defp validate_args(request_number, ip, port) do
    with :ok <- validate_request_number(request_number),
         :ok <- validate_ip_address(ip),
         :ok <- validate_port_number(port),
         do: :ok
  end

end
