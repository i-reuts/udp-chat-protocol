defmodule Core.Messages.FormatA do 
  @moduledoc """
  Shared encoders and decoders for messages with body format A.

  Body format A: <<request_number>>.
  """
  import Core.Utilities

  require Logger 

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct, atom) :: {:ok, binary} | {:error | String.t}
  def encode(message, type) do
    Logger.debug("Encoding #{type} message with RQ#: #{message.request_number}")

    case validate_request_number(message.request_number) do
      :ok -> 
        header = get_message_header(type)
        message = <<header::size(8), message.request_number::size(8)>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, map}
  def decode(message) do
    <<request_number::size(8)>> = message
    {:ok, %{request_number: request_number}}
  end

end
