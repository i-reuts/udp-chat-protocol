defmodule Core.Messages.FormatC do 
  @moduledoc """
  Shared encoders and decoders for messages with body format C.

  Body format C: <<request_number, username, status, port, allowed_users>>.
  """
  import Core.Utilities

  require Logger

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct, atom) :: {:ok, binary} | {:error | String.t}
  def encode(message, type) do
    Logger.debug("Encoding a #{type} message with RQ#: #{message.request_number}, username: #{message.username}, 
                 port: #{message.port}, status: #{message.status}, allowed_users: #{inspect message.allowed_users}")

    case process_args(message.request_number, message.username, message.port, message.status, message.allowed_users) do
      {:ok, name_size, status, allowed_users_binary} -> 
        header = get_message_header(type)
        message = <<header::size(8), message.request_number::size(8), name_size::size(8), message.username::binary-size(name_size), message.port::size(16), status::size(8), allowed_users_binary::binary>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end
  
  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(binary) :: {:ok, map}
  def decode(message) do
    <<request_number::size(8), name_size::size(8), username::binary-size(name_size), port::size(16), status::size(8), allowed_users_binary::binary>> = message
    allowed_users = :erlang.binary_to_term(allowed_users_binary)
    {:ok, %{request_number: request_number, username: username, port: port, 
            status: integer_to_status(status), allowed_users: allowed_users}}
  end

  @spec process_args(byte, String.t, :inet.port_number, String.t, list) :: 
    {:ok, integer, byte, binary} | {:error, String.t}
  defp process_args(request_number, username, port, status, allowed_users) do
    with :ok           <- validate_request_number(request_number),
         :ok           <- validate_username(username),
         :ok           <- validate_port_number(port),
         {:ok,status}  <- parse_status(status),
         :ok           <- validate_user_list(allowed_users),
         do: {:ok, byte_size(username), status, :erlang.term_to_binary(allowed_users)}
  end

end		
