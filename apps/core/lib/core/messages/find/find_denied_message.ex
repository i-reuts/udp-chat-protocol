defmodule Core.Messages.FindDeniedMessage do 
  @moduledoc """
  A struct representing FindDeniedMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{FindDeniedMessage, FormatB}

  @enforce_keys [:request_number, :username]
  defstruct [:request_number, :username]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%FindDeniedMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%FindDeniedMessage{} = message) do
    FormatB.encode(message, :find_denied)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary):: {:ok, %FindDeniedMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatB.decode(message)
    {:ok, struct(FindDeniedMessage, decoded_message)}
  end

end
