defmodule Core.Messages.FindRespMessage do 
  @moduledoc """
  A struct representing FindRespMessage and its associated encoder/decoder.
  """
  import Core.Utilities

  alias Core.Messages.FindRespMessage

  @enforce_keys [:request_number, :username]
  defstruct [:request_number, :username, :status, :port, :ip_address]

  require Logger

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  def encode(%FindRespMessage{status: nil} = message) do
    Logger.debug("Encoding a FindRespMessage message with RQ#: #{message.request_number}, 
                 username: #{message.username}, port: #{message.port}, IP: #{inspect message.ip_address}")

    case process_args(message.request_number, message.username, message.ip_address, message.port) do
      {:ok, name_size} -> 
        header = get_message_header(:find_resp)
        ip_binary = encode_ip(message.ip_address)
        message = <<header::size(8), message.request_number::size(8), name_size::size(8), message.username::binary-size(name_size), ip_binary::binary-size(4), message.port::size(16)>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end
  @spec encode(struct) :: {:ok, binary} | {:error | String.t} 
  def encode(%FindRespMessage{} = message) do
    Logger.debug("Encoding a FindRespMessage message with RQ#: #{message.request_number}, 
                  username: #{message.username}, status: #{message.status}")

    case process_args(message.request_number, message.username, message.status) do
      {:ok, name_size, status} -> 
        header = get_message_header(:find_resp)
        message = <<header::size(8), message.request_number::size(8), name_size::size(8), message.username::binary-size(name_size), status::size(8)>>
        {:ok, message}
      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, map}
  def decode(message) do
    <<request_number::size(8), name_size::size(8), username::binary-size(name_size), remainder::binary>> = message
    
    case remainder do
      <<ip_binary::binary-size(4), port::size(16)>> -> 
        ip_address = decode_ip(ip_binary)
        {:ok, %FindRespMessage{request_number: request_number, username: username, 
                               ip_address: ip_address, port: port}} 
      <<status::size(8)>> ->
        {:ok, %FindRespMessage{request_number: request_number, username: username, status: integer_to_status(status)}}
    end

  end

  @spec process_args(byte, String.t, :inet.ip_address, :inet.port_number) :: {:ok, integer} | {:error, String.t}
  defp process_args(request_number, username, ip, port) do
    with :ok  <- validate_request_number(request_number),
         :ok  <- validate_username(username),
         :ok  <- validate_ip_address(ip),
         :ok  <- validate_port_number(port),       
         do: {:ok, byte_size(username)}
  end

  @spec process_args(byte, String.t, String.t) :: {:ok, integer, binary} | {:error, String.t}
  defp process_args(request_number, username, status) do
    with :ok           <- validate_request_number(request_number),
         :ok           <- validate_username(username),
         {:ok,status}  <- parse_status(status),
         do: {:ok, byte_size(username), status}
  end

end
