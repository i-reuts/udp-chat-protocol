defmodule Core.Messages.FindReqMessage do 
  @moduledoc """
  A struct representing FindReqMessage and its associated encoder/decoder.
  """
  import Core.Utilities

  alias Core.Messages.{FindReqMessage}

  @enforce_keys [:request_number, :sender_username, :username]
  defstruct [:request_number, :sender_username, :username]

  require Logger

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%FindReqMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%FindReqMessage{} = message) do
    Logger.debug("Encoding find req message with RQ#: #{message.request_number}, 
                  sender_username: #{message.sender_username}, username: #{message.username}")

    case validate_args(message.request_number, message.sender_username, message.username) do
      :ok -> 
        header = get_message_header(:find_req)
        name_size = byte_size(message.sender_username)
        message = <<header::size(8), message.request_number::size(8), name_size::size(8), message.sender_username::binary-size(name_size), message.username::binary>>
        {:ok, message}
      {:error, reason} -> 
        {:error, reason}
    end
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary):: {:ok, %FindReqMessage{}}
  def decode(message) do
    <<request_number::size(8), name_size::size(8), sender_username::binary-size(name_size), username::binary>> = message 
    {:ok, %FindReqMessage{request_number: request_number, sender_username: sender_username, username: username}}
  end

  @spec validate_args(byte, String.t, String.t) :: :ok | {:error, String.t}
  defp validate_args(request_number, sender_username, username) do
    with :ok <- validate_request_number(request_number),
         :ok <- validate_username(sender_username),   
         :ok <- validate_username(username),    
         do: :ok
  end

end
