defmodule Core.Messages.ReferMessage do
  @moduledoc """
  A struct representing ReferMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{ReferMessage, FormatD}

  @enforce_keys [:request_number, :ip_address, :port]
  defstruct [:request_number, :ip_address, :port]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%ReferMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%ReferMessage{} = message) do
    FormatD.encode(message, :refer)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %ReferMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatD.decode(message)
    {:ok, struct(ReferMessage, decoded_message)}
  end

end
