defmodule Core.Messages.UserNotRegisteredMessage do
  @moduledoc """
  A struct representing UserNotRegisteredMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{UserNotRegisteredMessage, FormatB}

  require Logger

  @enforce_keys [:request_number, :username]
  defstruct [:request_number, :username]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%UserNotRegisteredMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%UserNotRegisteredMessage{} = message) do
    FormatB.encode(message, :user_not_registered)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %UserNotRegisteredMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatB.decode(message)
    {:ok, struct(UserNotRegisteredMessage, decoded_message)}
  end

end
