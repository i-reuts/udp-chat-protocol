defmodule Core.Messages.DeregisteredMessage do
  @moduledoc """
  A struct representing DeregisteredMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{DeregisteredMessage, FormatA}

  @enforce_keys [:request_number]
  defstruct [:request_number]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%DeregisteredMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%DeregisteredMessage{} = message) do
    FormatA.encode(message, :deregistered)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %DeregisteredMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatA.decode(message)
    {:ok, struct(DeregisteredMessage, decoded_message)}
  end

end
