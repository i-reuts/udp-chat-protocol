defmodule Core.Messages.RegisterMessage do
  @moduledoc """
  A struct representing RegisterMessage and its associated encoder/decoder.
  """
  import Core.Utilities
  
  alias Core.UDP
  alias Core.Messages.RegisterMessage

  require Logger

  @enforce_keys [:request_number, :username]
  defstruct [:request_number, :username, :ip_address]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%RegisterMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%RegisterMessage{} = message) do
    Logger.debug("Encoding a register message with RQ#: #{message.request_number}, username: #{message.username}")

     case validate_args(message.request_number, message.username) do
      :ok 
        -> header = get_message_header(:register)
           message = <<header::size(8), message.request_number::size(8), message.username::binary>>
           {:ok, message}
      {:error, reason} ->
           {:error, reason}
    end
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(UDP.address, binary) :: {:ok, %RegisterMessage{}}
  def decode(_sender_address = {ip_address, _port}, message) do
    <<request_number::size(8), username::binary>> = message
    {:ok, %RegisterMessage{request_number: request_number, username: username, ip_address: ip_address}}
  end

  @spec validate_args(byte, String.t) :: :ok | {:error, String.t}
  defp validate_args(request_number, username) do
    with :ok  <- validate_request_number(request_number), 
         :ok  <- validate_username(username),    
         do: :ok
  end

end
