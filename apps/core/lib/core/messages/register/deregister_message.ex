defmodule Core.Messages.DeregisterMessage do 
  @moduledoc """
  A struct representing InformReqMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{DeregisterMessage, FormatB}

  @enforce_keys [:request_number, :username]
  defstruct [:request_number, :username]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%DeregisterMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%DeregisterMessage{} = message) do
    FormatB.encode(message, :deregister)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary):: {:ok, %DeregisterMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatB.decode(message)
    {:ok, struct(DeregisterMessage, decoded_message)}
  end

end
