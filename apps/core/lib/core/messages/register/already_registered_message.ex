defmodule Core.Messages.AlreadyRegisteredMessage do
  @moduledoc """
  A struct representing AlreadyRegisteredMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{AlreadyRegisteredMessage, FormatA}

  @enforce_keys [:request_number]
  defstruct [:request_number]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%AlreadyRegisteredMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%AlreadyRegisteredMessage{} = message) do
    FormatA.encode(message, :already_registered)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %AlreadyRegisteredMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatA.decode(message)
    {:ok, struct(AlreadyRegisteredMessage, decoded_message)}
  end

end
