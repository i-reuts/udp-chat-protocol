defmodule Core.Messages.RegisteredMessage do
  @moduledoc """
  A struct representing RegisteredMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{RegisteredMessage, FormatA}

  @enforce_keys [:request_number]
  defstruct [:request_number]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%RegisteredMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%RegisteredMessage{} = message) do
    FormatA.encode(message, :registered)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %RegisteredMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatA.decode(message)
    {:ok, struct(RegisteredMessage, decoded_message)}
  end

end
