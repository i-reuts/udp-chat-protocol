defmodule Core.Messages.RegisterDeniedMessage do
  @moduledoc """
  A struct representing RegisterDeniedMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{RegisterDeniedMessage, FormatD}

  @enforce_keys [:request_number, :ip_address, :port]
  defstruct [:request_number, :ip_address, :port]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%RegisterDeniedMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%RegisterDeniedMessage{} = message) do
    FormatD.encode(message, :register_denied)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %RegisterDeniedMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatD.decode(message)
    {:ok, struct(RegisterDeniedMessage, decoded_message)}
  end

end
