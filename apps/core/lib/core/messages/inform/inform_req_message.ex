defmodule Core.Messages.InformReqMessage do 
  @moduledoc """
  A struct representing InformReqMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{InformReqMessage, FormatB}

  @enforce_keys [:request_number, :username]
  defstruct [:request_number, :username]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%InformReqMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%InformReqMessage{} = message) do
    FormatB.encode(message, :inform_req)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary):: {:ok, %InformReqMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatB.decode(message)
    {:ok, struct(InformReqMessage, decoded_message)}
  end

end
