defmodule Core.Messages.InformRespMessage do 
  @moduledoc """
  A struct representing InformRespMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{InformRespMessage, FormatC}

  @enforce_keys [:request_number, :username, :port, :status, :allowed_users]
  defstruct [:request_number, :username, :port, :status, :allowed_users]

  @doc """
  Converts internal structure representation to an encoded binary message
  """
  @spec encode(%InformRespMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%InformRespMessage{} = message) do
    FormatC.encode(message, :inform_resp)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(binary) :: {:ok, %InformRespMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatC.decode(message)
    {:ok, struct(InformRespMessage, decoded_message)}
  end

end		 
