defmodule Core.Messages.PublishedMessage do 
  @moduledoc """
  A struct representing PublishedMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{PublishedMessage, FormatC}

  @enforce_keys [:request_number, :username, :port, :status, :allowed_users]
  defstruct [:request_number, :username, :port, :status, :allowed_users]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%PublishedMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%PublishedMessage{} = message) do
    FormatC.encode(message, :published)
  end
  
  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(binary) :: {:ok, %PublishedMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatC.decode(message)
    {:ok, struct(PublishedMessage, decoded_message)}
  end

end		
