defmodule Core.Messages.PublishMessage do 
  @moduledoc """
  A struct representing PublishMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{PublishMessage, FormatC}

  require Logger

  @enforce_keys [:request_number, :username, :status, :allowed_users]
  defstruct [:request_number, :username, :port, :status, :allowed_users]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(struct) :: {:ok, binary} | {:error | String.t}
  def encode(message) do
    FormatC.encode(message, :publish)
  end
  
  @doc """
  Converts encoded binary message to an internal structure representation.	
  """
  @spec decode(binary) :: {:ok, map}
  def decode(message) do
    {:ok, decoded_message} = FormatC.decode(message)
    {:ok, struct(PublishMessage, decoded_message)}
  end

end		
