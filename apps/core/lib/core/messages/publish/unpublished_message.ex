defmodule Core.Messages.UnpublishedMessage do
  @moduledoc """
  A struct representing UnpublishedMessage and its associated encoder/decoder.
  """
  alias Core.Messages.{UnpublishedMessage, FormatA}

  @enforce_keys [:request_number]
  defstruct [:request_number]

  @doc """
  Converts internal structure representation to an encoded binary message.
  """
  @spec encode(%UnpublishedMessage{}) :: {:ok, binary} | {:error | String.t}
  def encode(%UnpublishedMessage{} = message) do
    FormatA.encode(message, :unpublished)
  end

  @doc """
  Converts encoded binary message to an internal structure representation.
  """
  @spec decode(binary) :: {:ok, %UnpublishedMessage{}}
  def decode(message) do
    {:ok, decoded_message} = FormatA.decode(message)
    {:ok, struct(UnpublishedMessage, decoded_message)}
  end

end
