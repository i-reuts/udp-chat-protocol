defmodule Core.MessageEncoder do
  @moduledoc """
  A message encoder facade.
  """
  alias Core.Messages.{RegisterMessage, RegisteredMessage, AlreadyRegisteredMessage, RegisterDeniedMessage, DeregisterMessage, DeregisteredMessage}
  alias Core.Messages.{PublishMessage, PublishedMessage, UnpublishedMessage}
  alias Core.Messages.{InformReqMessage, InformRespMessage}
  alias Core.Messages.{FindReqMessage, FindRespMessage, FindDeniedMessage, ReferMessage}
  alias Core.Messages.{ChatMessage, ByeMessage}
  alias Core.Messages.{UserNotRegisteredMessage}

  require Logger

  @doc """
  Encodes `message` by finding a corresponding message encoder and delegating the actual encoding to it.

  Logs encoding errors and debug information.
  """
  @spec encode(struct) :: {:ok, binary} | {:error, String.t}
  def encode(message) do
    case _encode(message) do
      {:ok, message} ->
        Logger.debug("Message encoded: #{Base.encode16(message)}")
        {:ok, message}
      {:error, reason} ->
        Logger.error("Error while encoding message: #{reason}")
        {:error, reason}
    end
  end

  @spec _encode(struct) :: {:ok, binary} | {:error, String.t}
  defp _encode(%RegisterMessage{} = message) do
    RegisterMessage.encode(message)
  end
  defp _encode(%RegisteredMessage{} = message) do
    RegisteredMessage.encode(message)
  end
  defp _encode(%AlreadyRegisteredMessage{} = message) do
    AlreadyRegisteredMessage.encode(message)
  end
  defp _encode(%RegisterDeniedMessage{} = message) do
    RegisterDeniedMessage.encode(message)
  end
  defp _encode(%DeregisterMessage{} = message) do
    DeregisterMessage.encode(message)
  end
  defp _encode(%DeregisteredMessage{} = message) do
    DeregisteredMessage.encode(message)
  end
  defp _encode(%PublishMessage{} = message) do
    PublishMessage.encode(message)
  end
  defp _encode(%PublishedMessage{} = message) do
    PublishedMessage.encode(message)
  end
  defp _encode(%UnpublishedMessage{} = message) do
    UnpublishedMessage.encode(message)
  end
  defp _encode(%InformReqMessage{} = message) do
    InformReqMessage.encode(message)
  end
  defp _encode(%InformRespMessage{} = message) do
    InformRespMessage.encode(message)
  end
  defp _encode(%FindReqMessage{} = message) do
    FindReqMessage.encode(message)
  end
  defp _encode(%FindRespMessage{} = message) do
    FindRespMessage.encode(message)
  end
  defp _encode(%FindDeniedMessage{} = message) do
    FindDeniedMessage.encode(message)
  end
  defp _encode(%ReferMessage{} = message) do
    ReferMessage.encode(message)
  end
  defp _encode(%ChatMessage{} = message) do
    ChatMessage.encode(message)
  end
  defp _encode(%ByeMessage{} = message) do
    ByeMessage.encode(message)
  end
  defp _encode(%UserNotRegisteredMessage{} = message) do
    UserNotRegisteredMessage.encode(message)
  end
  defp _encode(message) do
    {:error, "Encoder for message #{inspect message} not found."}
  end

end
