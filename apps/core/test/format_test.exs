defmodule FormatTest do
  use ExUnit.Case

  alias Core.{MessageFactory, MessageEncoder}

  @max_username_length Application.get_env(:core, :max_username_length)

  test "FormatB encoder correctly validates arguments" do
    # Invalid request number
    mock_message = MessageFactory.build_inform_req(321, "Peter")
    assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
    # Username too long
    mock_message = MessageFactory.build_inform_req(13, "ThisUsernameIsWayTooLong")
    assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
  end

  test "FormatC encoder correctly validates arguments" do
    # Invalid request number
    mock_message = MessageFactory.build_inform_resp(321, "Peter", 6873, "on", ["Mary", "John"])
    assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
    # Username too long
    mock_message = MessageFactory.build_inform_resp(13, "ThisUsernameIsWayTooLong", 6873, "on", ["Mary", "John"])
    assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
    # Invalid port number
    mock_message = MessageFactory.build_inform_resp(13, "Peter", 68734, "on", ["Mary", "John"])
    assert MessageEncoder.encode(mock_message) === {:error, "Invalid port number"} 
    # Invalid status
    mock_message = MessageFactory.build_inform_resp(13, "Peter", 6873, "invalid_status", ["Mary", "John"])
    assert MessageEncoder.encode(mock_message) === {:error, "User status invalid, assign as either 'on' or 'off'"}
    # Invalid user list 
    mock_message = MessageFactory.build_inform_resp(13, "Peter", 6873, "on", ["ThisUsernameIsWayTooLong", "John"])
    assert MessageEncoder.encode(mock_message) === {:error, "Invalid entry in the user list"}
  end

  test "FormatD encoder correctly validates arguments" do
    # Invalid request number
    mock_message = MessageFactory.build_refer(321, {123,19,54,231}, 4040)
    assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
    # Invalid IP address
    mock_message = MessageFactory.build_refer(13, {123,19,256,231}, 4040)
    assert MessageEncoder.encode(mock_message) === {:error, "Invalid IP address"}
    # Invalid port number
    mock_message = MessageFactory.build_refer(13, {123,19,54,231}, 404013)
    assert MessageEncoder.encode(mock_message) === {:error, "Invalid port number"}  
  end

end
