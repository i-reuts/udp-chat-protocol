defmodule MessageTest do
  use ExUnit.Case

  alias Core.{MessageFactory, MessageEncoder, MessageDecoder}

  @max_username_length Application.get_env(:core, :max_username_length)
  @max_text_size Application.get_env(:core, :max_text_size)

  describe "encoding and decoding of registration messages" do
    test "register message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_register(13, "Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
      assert decoded_message.ip_address === {123,19,94,211}
    end

    test "registered message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_registered(132)
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
    end

    test "already_registered message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_already_registered(211)
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
    end

    test "register_denied message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_register_denied(13, {123,19,72,235}, 9879)
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.ip_address === mock_message.ip_address
      assert decoded_message.port === mock_message.port
    end

    test "deregister message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_deregister(13, "Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
    end

    test "deregistered message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_deregistered(132)
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
    end

    test "encoding register message with invalid arguments results in errors" do
      # Invalid request number
      mock_message = MessageFactory.build_register(321, "Peter")
      assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
      # Username too long
      mock_message = MessageFactory.build_register(13, "ThisUsernameIsWayTooLong")
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
    end
  end

  describe "encoding and decoding of publish messages" do
    test "publish message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_publish(13, "Peter", 6873, "on", ["Mary", "John"])
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
      assert decoded_message.port === mock_message.port
      assert decoded_message.status === mock_message.status
      assert decoded_message.allowed_users === mock_message.allowed_users
    end

    test "published message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_published(13, "Peter", 6873, "on", ["Mary", "John"])
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
      assert decoded_message.port === mock_message.port
      assert decoded_message.status === mock_message.status
      assert decoded_message.allowed_users === mock_message.allowed_users
    end

    test "unpublished message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_unpublished(132)
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
    end
  end

  describe "encoding and decoding of inform messages" do
    test "inform_req message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_inform_req(13, "Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
    end

    test "inform_resp message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_inform_resp(13, "Peter", 6873, "on", ["Mary", "John"])
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
      assert decoded_message.port === mock_message.port
      assert decoded_message.status === mock_message.status
      assert decoded_message.allowed_users === mock_message.allowed_users
    end
  end

  describe "encoding and decoding of find messages" do
    test "find_req message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_find_req(13, "Bob", "Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.sender_username=== mock_message.sender_username
      assert decoded_message.username === mock_message.username
    end

    test "find_resp message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_find_resp(13, "Peter", 6873, {127,0,0,1})
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
      assert decoded_message.port === mock_message.port
      assert decoded_message.ip_address === mock_message.ip_address

      # Construct a message from mock data
      mock_message = MessageFactory.build_find_resp(13, "Peter", "off")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
      assert decoded_message.status === mock_message.status
    end

    test "find_denied message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_find_denied(13, "Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
    end

    test "refer message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_refer(13, {123,19,72,235}, 9879)
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.ip_address === mock_message.ip_address
      assert decoded_message.port === mock_message.port
    end

    test "find_req message correctly validates arguments" do
      # Invalid request number
      mock_message = MessageFactory.build_find_req(321, "Peter", "John")
      assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
      # Sender username too long
      mock_message = MessageFactory.build_find_req(13, "ThisUsernameIsWayTooLong", "John")
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
      # Username too long
      mock_message = MessageFactory.build_find_req(13, "Peter", "ThisUsernameIsWayTooLong")
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
    end

    test "find_resp message correctly validates arguments (format 1)" do
      # Invalid request number
      mock_message = MessageFactory.build_find_resp(321, "Peter", 6873, {127,0,0,1})
      assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
      # Username too long
      mock_message = MessageFactory.build_find_resp(13, "", 6873, {127,0,0,1})
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
      # Invalid port number
      mock_message = MessageFactory.build_find_resp(13, "Peter", 68731, {127,0,0,1})
      assert MessageEncoder.encode(mock_message) === {:error, "Invalid port number"} 
      # Invalid IP address
      mock_message = MessageFactory.build_find_resp(13, "Peter", 6873, {127,-1,0,1})
      assert MessageEncoder.encode(mock_message) === {:error, "Invalid IP address"}
    end

    test "find_resp message correctly validates arguments (format 2)" do
      # Invalid request number
      mock_message = MessageFactory.build_find_resp(321, "Peter", "on")
      assert MessageEncoder.encode(mock_message) === {:error, "Request number number 321 is outside the allowed range"} 
      # Username too long
      mock_message = MessageFactory.build_find_resp(13, "ThisUsernameIsWayTooLong", "on")
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
      # Invalid status
      mock_message = MessageFactory.build_find_resp(13, "Peter", "invalid status")
      assert MessageEncoder.encode(mock_message) === {:error, "User status invalid, assign as either 'on' or 'off'"} 
    end
  end

  describe "encoding and decoding of utility messages" do
    test "user_not_registered message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_user_not_registered(13, "Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.request_number === mock_message.request_number
      assert decoded_message.username === mock_message.username
    end
  end

  describe "encoding and decoding of chat messages" do
    test "chat message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_chat("Peter", "Hello, this is Pete!")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.username === mock_message.username
      assert decoded_message.ip_address === {123,19,94,211}
      assert decoded_message.port === 4040
      assert decoded_message.text === mock_message.text
    end

    test "bye message is encoded and decoded correctly" do
      # Construct a message from mock data
      mock_message = MessageFactory.build_bye("Peter")
      # Encode and decode the message
      decoded_message = encode_and_decode(mock_message)
      # Verify decoded message
      assert decoded_message.username === mock_message.username
      assert decoded_message.ip_address === {123,19,94,211}
      assert decoded_message.port === 4040
    end

    test "chat message correctly validates arguments" do
      # Username too long
      mock_message = MessageFactory.build_chat("ThisUsernameIsTooLong", "Hello, this is Pete!")
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 

      # Text too long
      mock_message = MessageFactory.build_chat("Peter", long_text)
      assert MessageEncoder.encode(mock_message) === {:error, "Text cannot be empty or exceed the maximum size of #{@max_text_size}"}
    end

    test "bye message correctly validates arguments" do
      # Username too long
      mock_message = MessageFactory.build_bye("ThisUsernameIsTooLong")
      assert MessageEncoder.encode(mock_message) === {:error, "Username cannot be empty or exceed the maximum length of #{@max_username_length}"} 
    end
  end

  describe "encoding and decoding invalid messages results in errors" do
    test "encoding an unknown message returns an error" do
      message = %{some_data: "message data"}
      assert MessageEncoder.encode(message) === {:error, "Encoder for message #{inspect message} not found."} 
    end

    test "decoding an unknown message header returns an error" do
      message = <<9,0>>
      mock_sender_address = {{123,19,94,211}, 4040}
      assert MessageDecoder.decode(mock_sender_address, message) === {:error, "Unknown message header - message might be damaged"} 
    end

    test "decoding an unknown message header format returns an error" do
      message = 0x0900
      mock_sender_address = {{123,19,94,211}, 4040}
      assert MessageDecoder.decode(mock_sender_address, message) === {:error, "Invalid message header format - message might be damaged"} 
    end

    test "decoding an unknows message body returns an error" do
      message = <<101,5,7>>
      mock_sender_address = {{123,19,94,211}, 4040}
      assert MessageDecoder.decode(mock_sender_address, message) === {:error, "Invalid message body format - message might be damaged"} 
    end
  end

  defp encode_and_decode(message) do
    # Encode the message
    {:ok, encoded_message} = MessageEncoder.encode(message)
    # Decode the message
    mock_sender_address = {{123,19,94,211}, 4040}
    {:ok, decoded_message} = MessageDecoder.decode(mock_sender_address, encoded_message)
    decoded_message
  end

  defp long_text do
    "Donec augue sapien, consectetur vitae consequat accumsan, scelerisque et diam. 
     Quisque ut est in purus fermentum mollis non vel nulla. Praesent metus."
  end
end
