defmodule UDPTest do
  use ExUnit.Case

  alias Core.{UDP, Utilities}
  
  @localhost {127,0,0,1}
  @client_port 6379
  @server_port 4041

  test "UDP server sends and receives messages" do
    # Start UDP server
    UDP.start_link(@client_port)

    # Start mock server
    {:ok, _pid} = Task.start_link(__MODULE__, :run_mock_server, ["request", "reply"])

    # Send request
    :ok = UDP.send_message("request", {@localhost, @server_port})

    # Receive and validate reply
    {:ok, packet} = UDP.receive(1000)
    {_ip, _port, message} = packet
    assert message === "reply"

    # Get active port
    {:ok, port} = UDP.get_active_port()
    assert :ok === Utilities.validate_port_number(port)
  end

  test "UDP server reports an error when attempting to send an invalid packet" do
    UDP.start_link(@client_port)
    assert UDP.send_message({:tuple}, {@localhost, @server_port}) === {:error, "Failed to send packed: einval"}
  end

  def run_mock_server(validation_message, reply) do
    # Open socket
    {:ok, socket} = :gen_udp.open(4041, [:binary, active: false])

    # Receive and decode message
    {:ok, packet} = :gen_udp.recv(socket, 0)
    {ip, port, message} = packet

    # Validate message
    assert message === validation_message

    # Encode and send reply
    :ok = :gen_udp.send(socket, ip, port, reply)
  end
end
