defmodule UtilitiesTest do
  use ExUnit.Case

  import Core.Utilities

  test "parse_ip_string" do
    valid_ip = "153.19.24.78"
    assert parse_ip_string(valid_ip) === {:ok, {153,19,24,78}}

    invalid_ip = "127.256.17.94"
    assert parse_ip_string(invalid_ip) === {:error, "Invalid IP address"}
  end

  test "ip_to_string" do
    assert ip_to_string({153,19,24,78}) === "153.19.24.78"
  end

  test "parse_port_number" do
    valid_port = "4040"
    assert parse_port_number(valid_port) === {:ok, 4040}

    invalid_port = "67981"
    assert parse_port_number(invalid_port) === {:error, "Invalid port number"}

    non_integer_port = "port"
    assert parse_port_number(non_integer_port) === {:error, "Port argument is not a number"}
  end
end