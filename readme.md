# Udp Chat Protocol

## Description

A simple client/server chat protocol over UDP written in Elixir.

Currently in development.

## Project structure

This project is an umbrella project. It consists of 3 sub-projects located in the `apps` directory:

  * `Core` - a library project containing functionality used by both Client and Server, such as message definitions, coders and utility functions.
  * `Server` - chat Server application.
  * `Client` - chat Client application. 

## Installation

After pulling from the repository, make sure to run `mix deps.get` to install all latest dependencies.

To enable static code analysis, set HOME environment variable and run `mix dialyzer` from the root project folder.

## Running

  1. Start the server by running `mix run --no-halt` from the `server` project directory.
  2. Start the client by running `mix run --no-halt` from the `client` project directory.
  3. Enter commands using the CLI client. The list of implemented commands can be found in `README.md` file inside `client` project directory.
  4. The list of available server commands used to setup a server chain can be found in `README.md` file inside `server` project directory.

## Additional functions

Run `mix test` to execute all unit tests.

Run `mix dialyzer` to perform static type checking.

Run `mix credo` to perform code linting.

Run `mix docs` to generate html documentation from code.

Run `mix coveralls.html` to generate html test coverage report.
